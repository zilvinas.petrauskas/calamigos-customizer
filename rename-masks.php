<?php

$dir ='localhost/test';
$renameToDir = 'localhost/renamed';
$removeString = 'M0001_';

$files = scandir($dir);

foreach($files as $file){
    if (strpos($file, $removeString) !== false) {

        $oldName = $dir .'/'. $file;

        $newFileName = str_replace($removeString,'',$file);
        $newName = $renameToDir .'/'. $newFileName;
       
        

        rename($oldName, $newName);
        echo "Renamed: ".$newName."<br>";
        // break;
    }
}