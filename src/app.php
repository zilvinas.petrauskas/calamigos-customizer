<?php

	// IMPORTANT! USE customizer.php instead
	// IMPORTANT! USE customizer.php instead
	// IMPORTANT! USE customizer.php instead

	header('Content-Type: text/html; charset=utf-8');
	echo 'Current PHP version: ' . phpversion() . "\r\n";
	
	/* The 2 lines below are a workaround based on this SO answer: https://stackoverflow.com/questions/1282909/php-post-array-empty-upon-form-submission*/
	$rest_json = file_get_contents("php://input");
	$_POST = json_decode($rest_json, true);



	$orderNumber = rand(10000, 99999);
	$path = './orders/' . $orderNumber;
	
	if (!is_dir($path))
	{
		mkdir($path, 0777, true);
	}
	$orderText = $_POST['orderText'];
	
	
	$orderFile = fopen($path . '/' . $orderNumber . "_1.txt", "w");
	fwrite($orderFile, $orderText);
	fclose($orderFile);
	
	
	
	if (isset($_POST['masks']))
	{
		$masks = $_POST['masks'];
		
		foreach ($masks as $key => $value)
		{
			if ($masks[$key]['PNGFile'])
			{
				$data = base64_decode($masks[$key]['PNGFile']);
				file_put_contents($path . '/' . $orderNumber . '_' . $key . '.png', $data);
			}
			if ($masks[$key]['BMPFile'])
			{
				$data = base64_decode($masks[$key]['BMPFile']);
				file_put_contents($path . '/' . $orderNumber . '_' .$key . '.bmp', $data);
			}
		}
	}
?>