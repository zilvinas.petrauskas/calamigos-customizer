import { Main } from 'Main';
import { ITEMS_CONFIG, LOCALHOST_FALLBACK } from './view/constants';

declare const PRODUCT_ID: string; // this catches globally and then can be retrieved

export class Product{

    private id: string;
    private _main: Main;

    constructor(main?: Main){

        this._main = main;
        this.id = typeof PRODUCT_ID !== 'undefined' && PRODUCT_ID !== null ? PRODUCT_ID : LOCALHOST_FALLBACK.PRODUCT_ID; // for localhost testing need a fallback id when shopify doesnt provide one

        console.log('PRODUCT ID', this.id);
    }

    public getProductId(){
        return this.id; // this is handle
    }
    public getSize(){
        const options = document.querySelectorAll('ul.color-false[data-option="option1"] > li.active');
        console.log('size options?', options);
        return options[0].getAttribute('data-text'); // data-text -> XS, S, M, L, XL
    }

    public getItemConfig(){

		const code = this.getProductId().toUpperCase();
		if(ITEMS_CONFIG.hasOwnProperty(code)){
			return ITEMS_CONFIG[code];
		}
		return null;
	}

    public isBlanket(){
        return this.id.search('CCB') > -1;
    }

    public isSweater(){
        return this.id.search('CC0') > -1;
    }

    public variant(){
        var url = new URL(window.location.href);
        var variant = url.searchParams.get("variant") || '';
        return variant;
    }

    public screenshot(){

        const resizeEnabled = true;
        const max = { // fit resize within these bounds
            width: 400,
            height: 400
        }	

        let screenshot = '';

        // save current camera position
        const oldPosition = this._main._scene.camera.position.clone();

         // change camera position for 1 frame
         if(this.isSweater()){
            this._main._scene.camera.position.set(0, 50, 550);
        }else{
            this._main._scene.camera.position.set(0, 110, 1250);
        }

        Main.instance._scene.triggerRender();
		const canvas:any = document.getElementById('myCanvas');
	        
        let resized:any = {};

        if(resizeEnabled){
            /* fit canvas into these bounds */
            	  		
            let canvasCopy = document.createElement("canvas")
            const copyContext = canvasCopy.getContext("2d")
           

            if(canvas.width / canvas.height > max.width / max.height) { 
                resized.width = max.width;
                resized.height = (canvas.height * (max.width / canvas.width)).toFixed(0);
            }else { 
                resized.width = (canvas.width * (max.height / canvas.height)).toFixed(0);
                resized.height = max.height;
            }

            canvasCopy.width = resized.width
            canvasCopy.height = resized.height;
            copyContext.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, canvasCopy.width, canvasCopy.height);

            screenshot = canvasCopy.toDataURL("image/png"); // WITH resize return resized canvas image
            /* end of resize */

        }else{
            screenshot = canvas.toDataURL("image/png"); // without resize return original canvas
        }
		

       
        
        // once screenshot is captured, restore camera position
        this._main._scene.camera.position.copy(oldPosition);

		return {
            image: screenshot,
            width: resized.width || max.width,
            height: resized.height || max.height,
        }

    }

    
}