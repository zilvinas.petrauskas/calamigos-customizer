import { SceneManager } from "./view/SceneManager";
import { Object3D, PerspectiveCamera, WebGLRenderer, Texture, TextureLoader, EquirectangularReflectionMapping, CanvasTexture, Color, Vector2, MeshStandardMaterial, Vector3, BoxBufferGeometry, Mesh, ObjectSpaceNormalMap, PointLight } from 'three';
import { PNG2BMPConverter } from 'model/PNG2BMPConverter';
import { BoundedConvergence } from 'utils/BoundedConvergence';
import { HTML } from 'utils/HTML';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrderManager } from 'model/OrderManager';
import { SWEATER_3D_MODEL, LOCALHOST_FALLBACK, SWEATER_CONFIG, BACKGROUND_COLOR_START, BACKGROUND_COLOR_END, AO_MAP_INTENSITY } from './view/constants';
import { Main } from 'Main';
import { ProgressBar } from './utils/ProgressBar';
import { IMasks } from './view/ColorControls';
import { GrassEnviroment } from 'view/GrassEnviroment';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as dat from 'three/examples/js/libs/dat.gui.min.js';
import { Rope } from 'view/Rope';

// creating fallbacks to work on localhost quicker and without a need to upload things online on every change

const testingBumpMaps = true;

// would be nice if sweater also had its class

export class ProductSweater{

    private _sceneContainer: Object3D;
	private _camera: PerspectiveCamera;
	private _light1: PointLight;
	private _renderer: WebGLRenderer;
	private _scene: SceneManager;
	private _whiteTexture: CanvasTexture;
	private _masks: IMasks;
	// private _isLoadFinished: boolean;
	private _PNG2BMPConverter: PNG2BMPConverter;
	private _main: Main;
    public _orderManager: OrderManager;
    private _controls: OrbitControls;
    private _rope: Rope;

	private _textures : any;
    public progressBar: ProgressBar;
    private grassEnv: any;
    
    constructor(scene: SceneManager, main: Main){
      
        this._main = main;
		this._scene = scene;
        
		this._renderer = scene.renderer;

		this._sceneContainer = new Object3D();
		this._scene.container.add(this._sceneContainer);

		// this._isLoadFinished = false;
		this.progressBar = new ProgressBar(this._scene);
		this._whiteTexture = this.createWhiteTexture();

		this._scene._renderer.gammaInput = true;
        this._scene._renderer.gammaOutput = true;
        this._scene._renderer.shadowMap.enabled = true;

		this._rope = new Rope(this._main);

		this.initCamera();
		this.initControls();
        this.initModel();
        
	}

	private initCamera(){

		this._scene._camera = new PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 10000 );
        this._scene._camera.position.set( 0, 150, 1700 );
        this._camera =  this._scene._camera;

	}
	
	private initControls(){


		// using original orbitControls from three here
		this._controls = new OrbitControls( this._scene._camera, this._scene._renderer.domElement);
		this._controls.minPolarAngle = Math.PI * 0.4;
		this._controls.maxPolarAngle = Math.PI * 0.5;
		this._controls.minDistance = 200; // 400
		this._controls.maxDistance = 1800;
		this._controls.enableZoom = true;
		this._controls.enablePan = false;
		this._controls.enableDamping = true;
		this._controls.dampingFactor = 0.9;
		this._controls.autoRotate = !true;
		this._controls.autoRotateSpeed = 0.2;

	}
    

    public loadTexture(url: string){

		return new Promise<Texture>((resolve, reject) =>
		{
			const loader = new TextureLoader();
			loader.load(
				url,
				(texture) =>
				{
					resolve(texture)
				},
				undefined,
				(err) =>
				{
					resolve(null);
				}
			)
		});
	}

	private async loadTextures(){

        const _baseUrl = this._main.getBaseUrl();

		this._masks = {
			FRT: await this.loadTexture(this._main.getTextureUrl('FRT')),
			BCK: await this.loadTexture(this._main.getTextureUrl('BCK')),
			SLV: await this.loadTexture(this._main.getTextureUrl('SLV')),
        };
        
        this._textures = {
			//envMap: this.initEnvMap(`${_baseUrl}${SWEATER_CONFIG.envMap}`),
			//aoMap: await this.loadTexture(`${_baseUrl}${SWEATER_CONFIG.aoMap}`),
			//ribMap: await this.loadTexture(`${_baseUrl}${SWEATER_CONFIG.ribMap}`), // rib & cuff
            //collarMap: await this.loadTexture(`${_baseUrl}${SWEATER_CONFIG.collarMap}`),
            GRASS: await this.loadTexture(`${_baseUrl}grasslight-big_v2.jpg`).catch(this.textureNotFound),
            WOOD: await this.loadTexture(`${_baseUrl}wood_texture.png`).catch(this.textureNotFound),
			// bumpTest: new TextureLoader().load('bumptest9.png'),
			FRT_NORMAL: await this.loadTexture(this._main.getTextureUrl('FRT_NORMAL')).catch(this.normalMapNotFound),
			BCK_NORMAL: await this.loadTexture(this._main.getTextureUrl('BCK_NORMAL')).catch(this.normalMapNotFound),
			SLV_NORMAL: await this.loadTexture(this._main.getTextureUrl('SLV_NORMAL')).catch(this.normalMapNotFound),
		}	
		

		// console.log('masks?', this._masks);
		// console.log('textures?', this._textures);
	}

	private textureNotFound(err){
		console.warn('Texture failed to load', err);
	}

	private normalMapNotFound(err){
		console.warn('Normal map not found', err);
	}

	public getMaskbyName(name: string){
		if(this._masks[name]){
			return this._masks[name];
		}
		return null;
	}

	private async initModel(){

		this.progressBar.startAnimationLoop();

		if( ! this._main.silentLoaderEnabled) this.progressBar.makeVisible();

		await this.loadTextures();
        let _baseURL = this._main.getBaseUrl();
        
        this.initBackground();
        this.loadScene(_baseURL +''+ SWEATER_3D_MODEL);
       
	}

	private createWhiteTexture(){

		const canvas = document.createElement('canvas');
		canvas.width = 1024;
		canvas.height = 512;

		const ctx = canvas.getContext('2d');
		ctx.fillStyle = '#FFFFFF';
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		const texture = new CanvasTexture(canvas);
		texture.flipY = false;

		return texture;
	}

	private initEnvMap(url: string){

		const envMap = new TextureLoader().load(url);
		envMap.mapping = EquirectangularReflectionMapping;

		return envMap;
	}

	private initBackground(){

        // TODO - add green background here
		this.grassEnv = new GrassEnviroment(this._scene, this._main, this._textures);
		// this.grassEnv.lightConfig.directional.position = new Vector3(50, 200, -100);
		this.grassEnv.poleConfig.h = 362.5;
		this.grassEnv.poleConfig.y = 140;
		
        this.grassEnv.init();
	
	}

	private getFileExtension(url: string){
		return url.slice((url.lastIndexOf(".") - 1 >>> 0) + 2);
	}

	private loadScene(url: string){
	
		const extension = this.getFileExtension(url);

		if(extension.toLowerCase() == 'glb'){
			this.loadUsingGltfLoader(url);
		}else{
			console.warn('Wrong model extension, should be GLB. ',extension, url);
		}
		
		
	}

	private afterModelLoad(){

		/** This is a workaround for the following issues:
		 * - Semi-transparent objects appearing white in the basket, when loaded from localstorage
		 * - Objects with larger textures make the screen freeze for a moment when they first being rendered on the scene
		 * - I figured out that if the whole scene is rendered first, then these issues disappear */
		const camPos = this._camera.position.clone();
		this._camera.position.set(0, 0, 20);
		this._camera.lookAt(0, 0, 0);
		this._renderer.render(this._scene.scene, this._camera);

		this.progressBar.finish();

		this._camera.position.copy(camPos);	

	}

	private loadUsingGltfLoader(url: string){

		const gltfLoader = new GLTFLoader();			

		gltfLoader.load(url, (gltf: GLTF) =>{
			this.onModelLoad(gltf);
			

		}, this.onProgress);

	}

	private onProgress = (progressEvent: ProgressEvent) =>{

		if (progressEvent.lengthComputable){
			this.progressBar.update(progressEvent.loaded,progressEvent.total)
		}

	};

	
	// currently only sweaters have model and garmet scene is created thru code
	// if more models will be loaded, need to separate things from each other and store textures Data somewhere
	private onModelLoad = (gltf: GLTF) =>{


		const sweater = gltf.scene; //.children[0];
		const _whiteTexture = this._whiteTexture.clone();
		
		
		const envMap = this._textures.envMap;

		// const aoMap = this._textures.aoMap;
		const ribMap = this._textures.ribMap;
		const collarMap = this._textures.collarMap;
		const aoMap = null;
		// const ribMap = null;
		// const collarMap = null;


		const normalMaps = {
			FRT : this._textures.FRT_NORMAL,
			BCK : this._textures.BCK_NORMAL,
			SLV : this._textures.SLV_NORMAL,
		}
		

		sweater.traverse((node: any) =>
		{

			if(node.type === 'Group'){
				node.traverse((node2:any) => {
					if(node2.isMesh){
						modifyMesh(node2, true);
					}
				});
			}else if(node.isMesh){
				modifyMesh(node);
			}

		});

		sweater.castShadow = true;
		sweater.receiveShadow = true;

        let _position = new Vector3(0,0,0);
        if(typeof this.grassEnv !== 'undefined'){
            // _position = new Vector3(0,142,-5); // cant zoom in to check things because its way up
            _position = new Vector3(0,60.5,-7);
        }

		// new shirt needs adjustments in scale and position
		let _scale = 40; //0.2;
		
		if( ! SWEATER_3D_MODEL.includes('new_')){ // old model
			_scale = 250;
			_position = new Vector3(0,0,0);
		}

		sweater.scale.set(_scale,_scale,_scale);
		sweater.position.copy(_position);

        // this._sceneContainer.add(sweater);
		this._scene.scene.add(sweater);
		
		// add rope
		// const ropeMesh = this._rope.forSweater();
		// this._scene.scene.add(ropeMesh);

		if (window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1"){
			
			// const gui = new dat.GUI();
		
			// gui.add(sweater.position, 'x', -200, 250).name('Sweater X Pos').step(1).onChange((val)=>{
			// 	sweater.position.x = val;
			// });
			// gui.add(sweater.position, 'y', -200, 250).name('Sweater Y Pos').step(1).onChange((val)=>{
			// 	sweater.position.y = val;
			// });
			// gui.add(sweater.position, 'z', -200, 250).name('Sweater Z Pos').step(1).onChange((val)=>{
			// 	sweater.position.z = val;
			// });
			
		}

		this._PNG2BMPConverter = new PNG2BMPConverter(sweater, this._masks, this._whiteTexture, this._main);

		this._orderManager = new OrderManager(this._PNG2BMPConverter, this._main);

		this.afterModelLoad();

		function modifyMesh(_node, applyBump: boolean = false){

			_node.castShadow = true; // enable shadows

			// separate and create unique material for each node
			var material2 = _node.material.clone();
			
			if (!_node.material.map) material2.map = _whiteTexture.clone();

			//material2.envMap = envMap;

			if(normalMaps[_node.name]){
				// material2.bumpMap = normalMaps[_node.name];
				// material2.bumpScale = 2;
				material2.normalMap = normalMaps[_node.name];
				material2.normalMap.flipY = false;
				// console.log('-- NORMAL MAP APPLIED for',_node.name);
			}

			if(isRibOrCuff(_node.name)){
				if(typeof ribMap !== 'undefined' && ribMap !== null){
					//material2.aoMap = ribMap;
					//material2.aoMapIntensity = AO_MAP_INTENSITY;
				}
			}else if(isCollar(_node.name)){
				if(typeof collarMap !== 'undefined' && collarMap !== null){
					//material2.aoMap = collarMap;
					//material2.aoMapIntensity = AO_MAP_INTENSITY;
				}
			}else{
				if(typeof aoMap !== 'undefined' && aoMap !== null){
					material2.aoMap = aoMap;
					material2.aoMapIntensity = AO_MAP_INTENSITY;
					// console.log('AO MAP APPLIED');
				}
			}

			

			material2.roughness = 0.895;


			
			// if(applyBump === true && _node.name === 'shirt_2' ){ // frt fixMeshName
			if(applyBump === true && _node.name === 'FRT' ){ // frt
				// material2.normalMap = bumpTest;
				// material2.bumpScale = 1;
				// material2.displacementMap = bumpTest;
				// material2.displacementScale = 1;
				// material2.displacementBias = 0;
			}
			

			_node.material = material2.clone();
			_node.material.needsUpdate = true;
		}

		function isRibOrCuff(_name){
			const lowerCaseName = _name.toLowerCase();
			return lowerCaseName.includes('rib') ||  lowerCaseName.includes('cuf');
		}
		function isCollar(_name){
			const lowerCaseName = _name.toLowerCase();
			return lowerCaseName.includes('clr');
		}
	};

	public animate(){
		if(this._controls) this._controls.update();
	}

	public showToolWhenLoadIsComplete(){
		
	}
}