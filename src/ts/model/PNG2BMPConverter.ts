import { ColorControls } from 'view/ColorControls';
import { SceneManager } from 'view/SceneManager';
import { CanvasTexture, Texture, Color } from 'three';
import { IMasks } from 'view/ColorControls';
import { COLOR_NAMES } from './../view/constants';
import { Main } from 'Main';

interface IMask
{
	PNGFile: string; // base64
	BMPFile: string; // base64
	colors: string[]; // hex
	label?: string; // FRT, BCK, or SLV
}

export class PNG2BMPConverter
{
	private _masks: {
		FRT: IMask,
		BCK: IMask,
		SLV: IMask,
		BLANKET: IMask,
		// [propName: string]: any,
	};
	private _inputElement: HTMLInputElement;
	private _downloadBtn: HTMLDivElement;
	private _canvas: HTMLCanvasElement;
	private _width: number;
	private _height: number;
	private _ctx: CanvasRenderingContext2D;
	private _fileName: string;
	private _newestMask: IMask;

	private _3DModel: THREE.Object3D;

	private _colorControls: ColorControls;
	private _main: Main;

	// added 2 new variable
	private _type: any;
	private _glCanvas: any;

	constructor(object: THREE.Object3D, masks: IMasks, whiteTexture: Texture, main: Main)
	{
		this._main = main;
		this._3DModel = object;
		this._newestMask = {
			PNGFile: null,
			BMPFile: null,
			colors: null,
		};
		this._masks = {
			FRT: {
				PNGFile: null,
				BMPFile: null,
				colors: null,
			},
			BCK: {
				PNGFile: null,
				BMPFile: null,
				colors: null,
			},
			SLV: {
				PNGFile: null,
				BMPFile: null,
				colors: null,
			},
			BLANKET: {
				PNGFile: null,
				BMPFile: null,
				colors: null,
			}
		};

		this._colorControls = new ColorControls(this, object, this._main);

		for (const mask in masks){
			const texture = masks[mask].image ? masks[mask] : whiteTexture;
			this.putImageOnModel(mask, texture.image);
		}

		// add 'active' class name on color picker
		this._colorControls.detectActiveColors();
		

		// this._downloadBtn = <HTMLDivElement>document.getElementById('downloadBMP');
		// this._inputElement = <HTMLInputElement>document.getElementById('inputPNG');
		// this._inputElement.addEventListener('change', (event: Event) =>
		// {
		// 	const file = (<any>event).currentTarget.files[0];
		// 	const PNGReader = new FileReader();

		// 	PNGReader.onload = (event: Event) =>
		// 	{
		// 		const img = new Image();
		// 		img.onload = () =>
		// 		{
		// 			this._canvas = document.createElement('canvas');
		// 			this._width = this._canvas.width = img.width;
		// 			this._height = this._canvas.height = img.height;

		// 			this._ctx = this._canvas.getContext('2d');
		// 			this._ctx.drawImage(img, 0, 0);



		// 			for (const type in this._masks)
		// 			{
		// 				if (file.name.includes(`${type}`))
		// 				{
		// 					this._masks[type].PNGFile = this._newestMask.PNGFile;
		// 					this._newestMask.label = type;

		// 					// const canvas = document.createElement('canvas');
		// 					// canvas.width = img.width;
		// 					// canvas.height = img.height;
		// 					// const ctx = canvas.getContext('2d');
		// 					// ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

		// 					const texture = new CanvasTexture(this._canvas);
		// 					const mesh = SceneManager.getObjectByMaterialName(this._3DModel, `${type}`);
		// 					if (mesh)
		// 					{
		// 						(<any>mesh).material.map = texture;
		// 						(<any>mesh).material.map.flipY = false;
		// 						(<any>mesh).material.needsUpdate = true;
		// 					}
		// 					break;
		// 				}
		// 			}

		// 			const fileName = file.name.split('.');
		// 			fileName.pop();
		// 			this._fileName = '';
		// 			for (let i = 0; i < fileName.length; ++i)
		// 			{
		// 				this._fileName += fileName[i];
		// 				this._fileName += '.';
		// 			}7

		// 			const BMPFile = this.getBMPFileFromImageData(this._newestMask.label);
		// 			const BMPReader = new FileReader();
		// 			BMPReader.onload = (event: Event) =>
		// 			{
		// 				this._newestMask.BMPFile = (<any>event).target.result.substr(22); /** We don't need this part: `data:image/bmp;base64,` */

		// 				for (const type in this._masks)
		// 				{
		// 					if (file.name.includes(`${type}`))
		// 					{
		// 						this._masks[type].BMPFile = this._newestMask.BMPFile;
		// 					}
		// 				}

		// 				this._downloadBtn.classList.remove('disabled');
		// 				this._downloadBtn.onclick = () =>
		// 				{
		// 					this.downloadFile(BMPFile);
		// 				};
		// 			};
		// 			BMPReader.readAsDataURL(BMPFile);
		// 		};
		// 		img.src = (<any>event).target.result;
		// 		this._newestMask.PNGFile = img.src.substr(22); /** We don't need this part: `data:image/png;base64,` */;
		// 	};

		// 	if (file)
		// 	{
		// 		PNGReader.readAsDataURL(file);
		// 	}
		//});
	}

	public changeColorOfModel(name: string, color: string){


		// TODO - check if image doesnt have logos/imprints 

		this._canvas = document.createElement('canvas');
		this._width = this._canvas.width = 512;
		this._height = this._canvas.height = 512;
		this._ctx = this._canvas.getContext('2d');
		this._ctx.fillStyle = '#'+color;
		this._ctx.fillRect(0,0, this._width, this._height);

		const texture = new CanvasTexture(this._canvas);
		const mesh = SceneManager.getObjectByName(this._3DModel, name);
		if (mesh){
			(<any>mesh).material.map = texture;
			(<any>mesh).material._color = color; // need this, because canvas doesnt have color property we can retrieve later (in getColorOfModelPart requested in OrderManager )
			(<any>mesh).material.needsUpdate = true;
		}else{
			console.log('---FAILED COLOR CHANGE FOR', name);
		}

	}

	public getColorOfModelPartAndAddItsName(name){
		const fixedName = this._colorControls.fixMeshName(name);
		const mesh = SceneManager.getObjectByName(this._3DModel, fixedName);
		if(mesh){
			if(mesh.material && mesh.material._color){
				let color = mesh.material._color;
				return `${color} (${COLOR_NAMES[color]})`; // qqli COLOR_NAMES
			}
		}
		return '';
	}

	public getColorOfModelPart(name){

		const fixedName = this._colorControls.fixMeshName(name);
		const mesh = SceneManager.getObjectByName(this._3DModel, fixedName);
		if(mesh){
			if(mesh.material && mesh.material._color){
				return mesh.material._color;
			}
		}
		return '';
	}

	public exportCustomColors(){
		return this._colorControls.exportCustomColors();
	}

	// section -> (string) (FRT, BCK, SLV)
	private putImageOnModel(section: string, img: HTMLImageElement | HTMLCanvasElement)
	{
		this._canvas = document.createElement('canvas');
		this._width = this._canvas.width = img.width;
		this._height = this._canvas.height = img.height;

		this._ctx = this._canvas.getContext('2d');
		this._ctx.drawImage(img, 0, 0);

		const texture = new CanvasTexture(this._canvas);
		
		const name = this._colorControls.fixMeshName(section);

		const mesh = SceneManager.getObjectByName(this._3DModel, name);
		
		if (mesh){
			(<any>mesh).material.map = texture;
			(<any>mesh).material.map.flipY = false;
			(<any>mesh).material.needsUpdate = true;
		}else{
			console.log('---DIDNT FIND', name, this._3DModel);
		}

		this.getBMPFileFromImageData(section, this._canvas); // TODO - it returns a file, but doesnt do shit with it?
	}

	public analyzeImgData = (imgData: Uint8ClampedArray) =>{
		const arrayOfColors = [];

		for (let i = 0; i < imgData.length; i += 4){
			const r = imgData[i];
			const g = imgData[i + 1];
			const b = imgData[i + 2];
			const a = imgData[i + 3];

			if (a === 255){
				const hex = ColorControls.rgbToHex(r, g, b);
				if (!arrayOfColors.includes(hex)){
					arrayOfColors.push(hex);
				}
			}
		}

		// console.log('arrayOfColors',arrayOfColors);

		// if (arrayOfColors.length > 3)
		// {
		// 	alert(`Uploaded PNG has more than 3 colors!`);
		// 	return null;
		// }
		// else
		{
			return arrayOfColors;
		}
	};

	private makeDataCompatible = (imgData: Uint8ClampedArray) =>{
		/** Removes the alpha and flips the results */
		const result = new Uint8ClampedArray(imgData.length * 3 / 4);

		let counter = 0;
		
		for (let y = this._height - 1; y >= 0; --y)
		{
			for (let x = 0; x < this._width; ++x)
			{
				const dataIndex = (y*this._width + x)*4;
				result[counter++] = imgData[dataIndex]; // R
				result[counter++] = imgData[dataIndex + 1]; // G
				result[counter++] = imgData[dataIndex + 2]; // B
			}
		}

		return result;
	};

	private getBMPFileFromImageData = (type: string, canvas: HTMLCanvasElement = this._canvas, updateColors: boolean = true) =>
	{
		const transparency = false;
		const colorOffset = transparency ? 4 : 3;

		const width = canvas.width;
		const height = canvas.height;

		const ctx = canvas.getContext('2d');

		const pngImageData = ctx.getImageData(0, 0, width, height).data;

		if (updateColors){ 
			const arrayOfColors = this.analyzeImgData(pngImageData);
			this._masks[type].colors = arrayOfColors;
			this._colorControls.saveColorsArray(type, arrayOfColors, canvas);
		}

		const imgData = this.makeDataCompatible(pngImageData);

		const headers = [];
		headers.length = 13;

		let extrabytes = 4 - ((width * colorOffset) % 4);   // How many bytes of padding to add to each
	                                                        // horizontal line - the size of which must
	                                                        // be a multiple of 4 bytes.
		if (extrabytes === 4)
		{
			extrabytes = 0;
		}

		const paddedsize = ((width * colorOffset) + extrabytes) * height;

		// Headers...
		// Note that the "BM" identifier in bytes 0 and 1 is NOT included in these "headers".

		headers[0] = paddedsize + 54;      // bfSize (whole file size)
		headers[1] = 0;                    // bfReserved (both)
		headers[2] = 54;                   // bfOffbits
		headers[3] = 40;                   // biSize
		headers[4] = width;                // biWidth
		headers[5] = height;               // biHeight

		// Would have biPlanes and biBitCount in position 6, but they're shorts.
		// It's easier to write them out separately (see below) than pretend
		// they're a single int, especially with endian issues...

		headers[7]  = 0;                    // biCompression
		headers[8]  = paddedsize;           // biSizeImage
		headers[9]  = 0;                    // biXPelsPerMeter
		headers[10] = 0;                    // biYPelsPerMeter
		headers[11] = 0;                    // biClrUsed
		headers[12] = 0;                    // biClrImportant

		//
		// Headers begin...
		// When printing ints and shorts, we write out 1 character at a time to avoid endian issues.
		//

		const img = new Uint8Array(headers[0]);

		img[0] = 66; // 'B'
		img[1] = 77; // 'M'

		for (let n = 0; n <= 5; n++)
		{
			img[n * 4 + 2] = headers[n] & 0x000000FF;
			img[n * 4 + 3] = (headers[n] & 0x0000FF00) >> 8;
			img[n * 4 + 4] = (headers[n] & 0x00FF0000) >> 16;
			img[n * 4 + 5] = (headers[n] & 0xFF000000) >> 24;
		}

		// These next 4 characters are for the biPlanes and biBitCount fields.

		img[26] = 1;
		img[27] = 0;
		img[28] = transparency ? 32 : 24;
		img[29] = 0;

		for (let n = 7; n <= 12; n++)
		{
			img[(n - 7) * 4 + 30] = headers[n] & 0x000000FF;
			img[(n - 7) * 4 + 31] = (headers[n] & 0x0000FF00) >> 8;
			img[(n - 7) * 4 + 32] = (headers[n] & 0x00FF0000) >> 16;
			img[(n - 7) * 4 + 33] = (headers[n] & 0xFF000000) >> 24;
		}

		//
		// Headers done, now write the data...
		//

		let toArrayCount = 0;
		let fromArrayCount = 0;

		for (let y = height - 1; y >= 0; --y)     // BMP image format is written from bottom to top...
		{
			for (let x = 0; x <= width - 1; ++x)
			{
				// Also it's in BGRA format
				img[54 + toArrayCount + 0] = imgData[fromArrayCount + 2];
				img[54 + toArrayCount + 1] = imgData[fromArrayCount + 1];
				img[54 + toArrayCount + 2] = imgData[fromArrayCount + 0];

				if (transparency)
				{
					img[54 + toArrayCount + 3] = imgData[fromArrayCount + 3];
				}

				toArrayCount += colorOffset;
				fromArrayCount += colorOffset;
			}
			if (extrabytes)      // See above - BMP lines must be of lengths divisible by 4.
			{
				for (let n = 1; n <= extrabytes; ++n)
				{
					img[54 + toArrayCount++] = 0;
				}
			}
		}

		const file = new File([img], `${this._fileName}bmp`, {
			type: 'image/bmp',
			lastModified: Date.now()
		});

		return file;
	};

	private downloadFileFromURL = (url: string, fileName: string) =>
	{
		const a = document.createElement('a');
		a.href = url;

		// This doesn't seem to be working, filename is always render.png
		a.setAttribute('download', fileName);
		a['download'] = fileName;

		a.style.display = 'none';
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	};

	private downloadFile = (file: File) =>
	{
		const url = URL.createObjectURL(file);
		this.downloadFileFromURL(url, file.name);
	};


	// each selection updates the canvas instead of continually saving the texture as PNG

	public updateMask(type: string, canvas: HTMLCanvasElement)
	{
		this._masks[type].PNGFile = canvas.toDataURL().substr(22);
		// const BMPFile = this.getBMPFileFromImageData(type, canvas, false);
		// const BMPReader = new FileReader();
		// BMPReader.onload = (event: Event) =>
		// {
		// 	this._masks[type].BMPFile = (<any>event).target.result.substr(22); /** We don't need this part: `data:image/bmp;base64,` */
		// }
		// BMPReader.readAsDataURL(BMPFile);
	}

	public getMasks()
	{
		
		var frontCanvas = <HTMLCanvasElement>document.getElementById("FRT");
		if(frontCanvas != null) {
			this.updateMask("FRT", frontCanvas);
		}

		var backCanvas = <HTMLCanvasElement>document.getElementById("BCK");
		if(backCanvas != null) {
			this.updateMask("BCK", backCanvas);
		}

		var sleeveCanvas = <HTMLCanvasElement>document.getElementById("SLV");
		if(sleeveCanvas != null) {
			this.updateMask("SLV", sleeveCanvas);
		}
		
		return this._masks;
	}
}