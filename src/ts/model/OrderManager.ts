import { PNG2BMPConverter } from './PNG2BMPConverter';
import { AJAX } from 'utils/AJAX';
import { Main } from './../Main';
import { EXTERNAL_SERVER_URL, COLOR_NAMES } from './../view/constants';

export class OrderManager
{

	private _PNG2BMPConverter: PNG2BMPConverter;
	private _main: Main;
	

	constructor(PNG2BMPConverter: PNG2BMPConverter, main: Main)
	{
		this._PNG2BMPConverter = PNG2BMPConverter;
		this._main = main;				

		// EVENT ADD TO CART
		window.addEventListener('cart:add', ()=>{
			console.log('event - cart:add');
			this.addToCart(); // OrderManager.ts
		});

		window.addEventListener('cart:remove', ()=>{
			console.log('event - cart:remove');
		});

		
	}

	public addToCart(){

		const productID = this._main.product.getProductId(); // this is actualla 'handle' not an product id in shopify admin
		const realID = document.getElementById('section-product').dataset.id;
		
		// send these to backend
		let params: any = {
			product: {
				id: productID, // CC001 e.g.
				realID : realID, // id used in shopify
				variant: this._main.product.variant(),
				quantity: 1,
				colors: {},
			},
			masks: this._PNG2BMPConverter.getMasks(),
			screenshot: this._main.product.screenshot(), // image, width, height
			
		};

		// save custom colors
		const customColors = this._PNG2BMPConverter.exportCustomColors();
		if(customColors && Object.keys(customColors).length > 0){
			for(let name in customColors){
				let colorName = COLOR_NAMES[customColors[name]];
				params.product.colors[name] = `${customColors[name]} (${colorName})`;
			}
		}
		

		if(this._main.product.isBlanket()){

			
			params.product.type = 'blanket';
			params.product.size = '';

		}else{
		
			// get product size
			params.product.size = this._main.product.getSize();

			// save colors of collar, cuff & rib
			params.product.colors.collar = this._PNG2BMPConverter.getColorOfModelPartAndAddItsName('CLR');
			params.product.colors.cuff = this._PNG2BMPConverter.getColorOfModelPartAndAddItsName('CUF');
			params.product.colors.rib = this._PNG2BMPConverter.getColorOfModelPartAndAddItsName('RIB');

			params.product.type = 'sweater';

		}

		// console.log('saving order', params);

		this.saveOrder(params);
	}

	private saveOrder(params){

		// probably you want to trigger your actual shopify add to cart fn too??
		let url;
		let isLocalhost = false;

		if (window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1"){

			// trigger local script on localhost
			url = this._main.getBaseUrl() +''+ './customizer.php';
			isLocalhost = true;

		}else if(typeof EXTERNAL_SERVER_URL !== 'undefined'){

			// send to external url on official server
			url = EXTERNAL_SERVER_URL;

		}else{
			console.warn('-- Define EXTERNAL_SERVER_URL at constants.ts!');
			return;
		}
		

		AJAX.load({
			url: url,
			params: params,
			method: 'POST',
			xhrFields: {
				withCredentials: true
			},
			onComplete: (jsonResponse) =>
			{
				const resp = JSON.parse(jsonResponse);
				console.log('order saved?', resp);

			
				// each product needs its own hidden attributes

				if(! isLocalhost ){

					const customCartAttributesHolder = document.getElementById('customCartAttributes');
					const customProduct:any= {};

					customProduct.handle = params.product.id;
					customProduct.productID = params.product.realID;
					customProduct.customOrder = resp.orderNumber;
					if(resp.screenshot) customProduct.screenshot = resp.screenshot;
					if(resp.colors) customProduct.colors = resp.colors;
					

					const customInput: any = document.getElementById('customizerCustomOrder');

					if(!customInput){
						console.warn('Create #customizerCustomOrder hidden input in cart_form');
					}

					const storageName = 'customizeCustomData';

					let existingData = window.localStorage.getItem(storageName);
					let newData = "";

					if(typeof existingData !== 'undefined' && existingData !== null){

						let array = JSON.parse(existingData);
						array.push(customProduct);
						newData = JSON.stringify(array);

					}else{

						let array = [];
						array.push(customProduct);
						newData = JSON.stringify(array);

					}
					
					window.localStorage.setItem(storageName, newData);
					customInput.setAttribute('value', newData);

					// custom item test
					// TODO - need to add variant into these
					const customColorsKey = `customColors_${params.product.realID}_${params.product.variant}`;
					const customItemImageKey = `customImage_${params.product.realID}_${params.product.variant}`; // cant do this, image doesnt exist yet in cart (cart in shopify didnt add things yet)
					

					// replace original product image
					window.localStorage.setItem(customItemImageKey, resp.screenshot);
					waitToReplaceImage(customItemImageKey, resp.screenshot);

					// create color boxes i ncart
					const saveCustomColors = JSON.stringify(resp.colors);
					window.localStorage.setItem(customColorsKey, saveCustomColors);

					const customColorsParent = document.getElementById(customItemImageKey);
					createCartProductColors(resp.colors, customColorsParent);
					

					const btnCancel = document.querySelectorAll('.customizerCancel')[0] as HTMLElement;
					btnCancel.click();
	
					// trigger shopify add to cart
					const btnCart = document.querySelectorAll('.cart-functions button[type="submit"]')[0] as HTMLElement;
					btnCart.click();

				}

				
			}
		});


		// <input id="customizercustomimage" type="hidden" name="attributes[customizerCustomImage]" value="{{ cart.attributes["customizerCustomImage"] }}">

		let createHiddenInput = (_name, _array:boolean=false) => {

			const _input = document.createElement('input');
			_input.setAttribute('type', 'hidden');

			if(_array){
				_input.setAttribute('name', `attributes[${_name}][]`);
			}else{
				_input.setAttribute('name', `attributes[${_name}]`);
			}
			

			return _input;
		}

		let createCartProductColors = (colors, customColorsParent) => {

			if(!customColorsParent){
				// customColorsParent = document.createElement('div');
				// customColorsParent.classList.add('cart-product-colors');
				console.log('missing custom colors parent');
				return;
			}

			// this below should match things in cart_form
			if(Object.keys(colors).length > 0){
				for(let i in colors){
					if(  !colors.hasOwnProperty(i)) continue;
					let colorDiv = document.createElement('div');
					colorDiv.classList.add('custom-product-color-preview');
					colorDiv.style.backgroundColor = "#"+colors[i];
					customColorsParent.appendChild(colorDiv);
				}
			}

		}

		let waitToReplaceImage = (_id, _src)=>{

			let maxWait = 5000;
			let currentWait = 0;
			
			function wait(){
				let elem = document.getElementById(_id);
				if(elem){
					elem.setAttribute('src',_src);
				}else{
					let t = 50;
					currentWait += t;
					if(currentWait < maxWait){
						setTimeout(()=>{
							wait();
						}, t);
					}else{
						console.log('FAILED to replace img',_id);
					}
				}
			}

			wait();

		}

	}


}