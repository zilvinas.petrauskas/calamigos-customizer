import { LOCALHOST_FALLBACK } from './view/constants';
import { Main } from './Main';
import { Product } from './Product';
import { SceneManager } from './view/SceneManager';
import { Scene, Fog, Object3D, Mesh, Vector3, SphereBufferGeometry, BoxBufferGeometry, Loader, Math as THREEMATH, MeshDepthMaterial, RGBADepthPacking, PlaneBufferGeometry, ObjectSpaceNormalMap, TangentSpaceNormalMap, RepeatWrapping, DoubleSide, ParametricBufferGeometry, MeshLambertMaterial, AmbientLight, DirectionalLight, PerspectiveCamera, WebGLRenderer, Texture, TextureLoader, EquirectangularReflectionMapping, CanvasTexture, Color, Vector2, MeshStandardMaterial, BoxGeometry, Group } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { PNG2BMPConverter } from 'model/PNG2BMPConverter';
import { OrderManager } from 'model/OrderManager';
import { ClothSimulator, Cloth } from 'utils/ClothSimulator';
import { ProgressBar } from 'utils/ProgressBar';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { DDSLoader } from 'three/examples/jsm/loaders/DDSLoader';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import * as dat from 'three/examples/js/libs/dat.gui.min.js';
import { gunzip } from 'zlib';
import { GrassEnviroment } from 'view/GrassEnviroment';
            
/* physics global stuff */
function plane( width, height ) {
    return function ( u, v, target ) {
        let x = ( u - 0.5 ) * width;
        let y = height - ( v + 0.5 ) * height;
        let z = 0;
        target.set( x, y, z );
    };

}
const restDistance = 25;
const xSegs = 10;
const ySegs = 15;
const clothFunction = plane( restDistance * xSegs, restDistance * ySegs );
/* end of pshysics global */

export class ProductGarmet{

   
    private _canvas: any;
	private _camera: PerspectiveCamera;
	private _renderer: WebGLRenderer;
	private _container: HTMLElement;
	private _scene: SceneManager;
	private _PNG2BMPConverter: PNG2BMPConverter;
	private _main: Main;
	public _orderManager: OrderManager;
    private _clothSimulator;
    private _clothGeometry;
    private _object;
    private _sphere;
    private _controls: OrbitControls;
    public progressBar: ProgressBar;
    private _world: Scene;
    private _sceneContainer: Object3D;
    private complete: Boolean;
	private _textures : any;
    private _masks : any;
    private physics;
    private addFlowers: boolean;
    private flowers: any;

    constructor(scene: SceneManager, main: Main){

        this.addFlowers = false; 

        this._main = main;
        this._scene = scene;
        this._camera = scene.camera;
        this._renderer = scene.renderer;
        this._sceneContainer = new Object3D();
        this._scene.container.add(this._sceneContainer);

        this.complete = false;
        this.progressBar = new ProgressBar(this._scene);
        this.progressBar.onCompleteCallback = this.onLoadComplete.bind(this);
        
        this._scene._renderer.gammaInput = true;
        this._scene._renderer.gammaOutput = true;
        this._scene._renderer.shadowMap.enabled = true;
                

        this.initCamera();
        this.initControls();
        
        this.progressBar.startAnimationLoop();
        if( ! this._main.silentLoaderEnabled) this.progressBar.makeVisible();
        this.initModelManuallyCreated();
        
    }

    // load 3d model
    private async initModel(){

        // not using this, creating blanket manually in code, because it requires parametric geometry I cant retrieve from 3d model
        // await this.loadTextures().catch(this.catchTextureLoadError);
        // let _baseURL = this._main.getBaseUrl();
		// this.loadModel(_baseURL +''+ BLANKET_3D_MODEL);
       
    }

    private initCamera(){
        this._scene._camera = new PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 10000 );
        this._scene._camera.position.set( 0, 150, 1700 );
    }

    private initControls(){


        this._controls = new OrbitControls( this._scene._camera, this._scene._renderer.domElement);
        this._controls.minPolarAngle = Math.PI * 0.4;
        this._controls.maxPolarAngle = Math.PI * 0.5;
        this._controls.minDistance = 800;
        this._controls.maxDistance = 1800;
        this._controls.enableZoom = true;
        this._controls.enableDamping = true;
        this._controls.dampingFactor = 0.9;
        this._controls.autoRotate = true;
        this._controls.autoRotateSpeed = 2.0;
        this._controls.autoRotate = false;
        this._controls.minAzimuthAngle = -Math.PI * 0.4;
        this._controls.maxAzimuthAngle = Math.PI * 0.4;
    }

    // create blanket model in code
    private async initModelManuallyCreated(){

        await this.loadTextures().catch(this.catchTextureLoadError);

        if(this.addFlowers) await this.loadFlowers().catch(this.failedLoadingFlowers);

        this.createScene(true);
        
        this._orderManager = new OrderManager(this._PNG2BMPConverter, this._main);
    }
   
    private catchTextureLoadError(err){
        console.log('load texture failed', err);
        alert('Product cannot be customized - missing a texture');
        document.getElementById('customizerProgressBar').classList.remove('loading');
    }


    private async loadFlowers(){

        this.flowers = {
            model: await this.loadFlowerModel(),
        }

    }

    private async loadFlowerModel(){

        let flowerDir = this._main.getBaseUrl();

        const progressBar = this.progressBar;

    
        let _baseURL = this._main.getBaseUrl();
        let url = _baseURL +'dandelions.glb';

        progressBar.set(50);

        // load it in glb - faster
        return new Promise<any>((resolve, reject) =>{
            const gltfLoader = new GLTFLoader();			

            gltfLoader.load(url, (gltf: GLTF) =>{
               
                const object = gltf.scene;
                progressBar.set(75);
                resolve(object);

            }, this.onProgress);
        });


    }
    

    private failedLoadingFlowers(err){
        console.log('load texture failed', err);
    }
   

    private loadModel(url: string){

        const gltfLoader = new GLTFLoader();			

		gltfLoader.load(url, (gltf: GLTF) =>{
			this.onModelLoad(gltf);
		}, this.onProgress);
    }

    private onModelLoad(gltf: GLTF){

        const _whiteTexture = this.createWhiteTexture();
        const aoMap = this._textures.AO;

        gltf.scene.traverse((node: any) =>{

            if(node.isMesh){

                const material2 = node.material.clone();
                
                if (!node.material.map) material2.map = _whiteTexture.clone();
                // material2.envMap = envMap;
                
                material2.aoMap = aoMap;
                material2.aoMapIntensity = 2; //1.2;
                
                node = material2.clone();
            }
		});

        const object = gltf.scene.children[0];

        this._PNG2BMPConverter = new PNG2BMPConverter(object, this._masks, _whiteTexture, this._main);

        this._object = object; // add this on scene later
        this._orderManager = new OrderManager(this._PNG2BMPConverter, this._main);
        
        this.afterModelLoad();

    }

    private createWhiteTexture(){

		const canvas = document.createElement('canvas');
		canvas.width = 1024;
		canvas.height = 512;

        const ctx = canvas.getContext('2d');
		ctx.fillStyle = '#FFFFFF';
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		const texture = new CanvasTexture(canvas);
		texture.flipY = false;

		return texture;
	}
    
    private afterModelLoad(){

        this.createScene();
    }

	private onProgress = (progressEvent: ProgressEvent) =>{

		if (progressEvent.lengthComputable){
			this.progressBar.update(progressEvent.loaded,progressEvent.total)
		}

    };

    private async loadTextures(){

        // qq
        const config = this._main.product.getItemConfig();
        const _baseURL = this._main.getBaseUrl();

        this._masks = {
            BLANKET: await this.loadTexture(this._main.getTextureUrl('BLANKET')).catch(this.textureFailedToLoad),
        };

        this._textures = {
            AO: await this.loadTexture(`${_baseURL}garmet_ao_v3.jpg`).catch(this.textureFailedToLoad),
            GRASS: await this.loadTexture(`${_baseURL}grasslight-big_v2.jpg`).catch(this.textureFailedToLoad),
            WOOD: await this.loadTexture(`${_baseURL}wood_texture.png`).catch(this.textureFailedToLoad),
            NORMAL_MAP: await this.loadTexture(this._main.getTextureUrl('BLANKET_NORMAL')).catch(this.textureFailedToLoad),
        };
        
    }

    public loadTexture(url: string){

		return new Promise<Texture>((resolve, reject) =>
		{
			const loader = new TextureLoader();
			loader.load(
				url,
				(texture) =>
				{
					resolve(texture)
				},
				undefined,
				(err) =>
				{
					reject(err);
				}
			)
		});
    }
    
    private textureFailedToLoad(err){
        console.log('texture failed to load', err);
    }

    private createScene(manual3dModel: boolean = false){

        this._world = this._scene.scene;

        const grassEnv = new GrassEnviroment(this._scene, this._main, this._textures);
        // can chance poleConfig, blanketConfig, groundConfig before init if needed
        grassEnv.init();

        let normalMap = this._textures.NORMAL_MAP;

        let clothTexture = this._masks.BLANKET;
        clothTexture.anisotropy = 16;

        if(typeof clothTexture === 'undefined' || clothTexture === null){
            console.warn('Cloth texture not found, not loading customizer');
            return;
        }
       
     
        // scene

        this._clothSimulator = new ClothSimulator();

      

        // cloth material
        
        const canvas = document.createElement('canvas');
		canvas.width = 1200;
		canvas.height = 1800; // keep 2/3 ratio

		const ctx = canvas.getContext('2d');
		ctx.fillStyle = '#FFFFFF';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(clothTexture.image, 0, 0, canvas.width, canvas.height );

		const canvasTexture = new CanvasTexture(canvas);
        canvasTexture.flipY = true;
        
        const clothMaterial = new MeshStandardMaterial( {

            name: 'BLANKET',
            color: new Color('#FFF'),
            map: canvasTexture, // must be a canvas, so it could be modified later on
            side: DoubleSide,
            alphaTest: 0.5,

            roughness: 0.89,
            refractionRatio: 0.98,

            bumpMap: this._textures.AO,
            bumpScale: 2,
            
        } );

        if(typeof normalMap !== 'undefined' && normalMap !== null){
            clothMaterial.normalMap = normalMap;
        }
        
        // cloth geometry

        const clothGeometry = new ParametricBufferGeometry( clothFunction, xSegs, ySegs );     
        this._clothGeometry = clothGeometry;

        // cloth mesh

        if(manual3dModel){

            const blanket = new Mesh( this._clothGeometry, clothMaterial );
            blanket.name = 'BLANKET';
            const blanketPosY = grassEnv.blanketConfig.y;
            blanket.position.set( 0, blanketPosY, 0 );
            blanket.castShadow = true;
            // blanket.rotation.y = Math.PI;

            this._object = blanket;
            
            this._world.add( this._object );

            this._PNG2BMPConverter = new PNG2BMPConverter(this._object, this._masks, this.createWhiteTexture(), this._main);
        }
       
        
        // sphere

        var ballGeo = new SphereBufferGeometry( ClothSimulator.ballSize, 32, 16 );
        var ballMaterial = new MeshLambertMaterial();

        this._sphere = new Mesh( ballGeo, ballMaterial );
        this._sphere.visible = false;
        this._sphere.castShadow = true;
        this._sphere.receiveShadow = true;
        this._world.add( this._sphere );
        
        this._clothSimulator.clothGeometry = this._clothGeometry;
        this._clothSimulator.sphere = this._sphere;
       
        if(this._sphere) this._sphere.visible = ! true;

        this.physics = this.initPhysics(this._clothGeometry, this._sphere);

        this.progressBar.finish();
    
    }
    
    //

    public animate() {

        if(!this.complete) return; // dont render while loading stuff in

        this.render();

    }

    private render() {

        if(this.physics){
            this.physics.animate();
        }

        this._renderer.render( this._scene.scene, this._scene._camera );

    }

    private onLoadComplete(){
        this.complete = true;
    }

    public showToolWhenLoadIsComplete(){
		
	}


    // public check(){
        // this.progressBar.animate();
    // }


    private initPhysics(clothGeometry, sphere){

        var params = {
            enableWind: true,
            showBall: false,
        };

        var DAMPING = 0.03;
        var DRAG = 1 - DAMPING;
        var MASS = 0.1;

        // var restDistance = 25;
        // var xSegs = 10;
        // var ySegs = 15;
        // var clothFunction = plane( restDistance * xSegs, restDistance * ySegs );

        var cloth = new Cloth( xSegs, ySegs );

        var GRAVITY = 981 * 1.4;
        var gravity = new Vector3( 0, - GRAVITY, 0 ).multiplyScalar( MASS );


        var TIMESTEP = 18 / 1000;
        var TIMESTEP_SQ = TIMESTEP * TIMESTEP;

        var pins = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];

        var windForce = new Vector3( 0, 0, 0 );

        var ballPosition = new Vector3( 0, - 45, 0 );
        var ballSize = 60; //40

        var tmpForce = new Vector3();

        var lastTime;


        // function plane( width, height ) {

        //     return function ( u, v, target ) {

        //         var x = ( u - 0.5 ) * width;
        //         var y = ( v + 0.5 ) * height;
        //         var z = 0;

        //         target.set( x, y, z );

        //     };

        // }

        function Particle( x, y, z, mass ) {

            this.position = new Vector3();
            this.previous = new Vector3();
            this.original = new Vector3();
            this.a = new Vector3( 0, 0, 0 ); // acceleration
            this.mass = mass;
            this.invMass = 1 / mass;
            this.tmp = new Vector3();
            this.tmp2 = new Vector3();

            // init

            clothFunction( x, y, this.position ); // position
            clothFunction( x, y, this.previous ); // previous
            clothFunction( x, y, this.original );

        }

        // Force -> Acceleration

        Particle.prototype.addForce = function ( force ) {

            this.a.add(
                this.tmp2.copy( force ).multiplyScalar( this.invMass )
            );

        };


        // Performs Verlet integration

        Particle.prototype.integrate = function ( timesq ) {

            var newPos = this.tmp.subVectors( this.position, this.previous );
            newPos.multiplyScalar( DRAG ).add( this.position );
            newPos.add( this.a.multiplyScalar( timesq ) );

            this.tmp = this.previous;
            this.previous = this.position;
            this.position = newPos;

            this.a.set( 0, 0, 0 );

        };


        var diff = new Vector3();

        function satisfyConstraints( p1, p2, distance ) {

            diff.subVectors( p2.position, p1.position );
            var currentDist = diff.length();
            if ( currentDist === 0 ) return; // prevents division by 0
            var correction = diff.multiplyScalar( 1 - distance / currentDist );
            var correctionHalf = correction.multiplyScalar( 0.5 );
            p1.position.add( correctionHalf );
            p2.position.sub( correctionHalf );

        }


        function Cloth( w, h ) {

            w = w || 10;
            h = h || 10;
            this.w = w;
            this.h = h;

            var particles = [];
            var constraints = [];

            var u, v;

            // Create particles
            for ( v = 0; v <= h; v ++ ) {

                for ( u = 0; u <= w; u ++ ) {

                    particles.push(
                        new Particle( u / w, v / h, 0, MASS )
                    );

                }

            }

            // Structural

            for ( v = 0; v < h; v ++ ) {

                for ( u = 0; u < w; u ++ ) {

                    constraints.push( [
                        particles[ index( u, v ) ],
                        particles[ index( u, v + 1 ) ],
                        restDistance
                    ] );

                    constraints.push( [
                        particles[ index( u, v ) ],
                        particles[ index( u + 1, v ) ],
                        restDistance
                    ] );

                }

            }

            for ( u = w, v = 0; v < h; v ++ ) {

                constraints.push( [
                    particles[ index( u, v ) ],
                    particles[ index( u, v + 1 ) ],
                    restDistance

                ] );

            }

            for ( v = h, u = 0; u < w; u ++ ) {

                constraints.push( [
                    particles[ index( u, v ) ],
                    particles[ index( u + 1, v ) ],
                    restDistance
                ] );

            }


            // While many systems use shear and bend springs,
            // the relaxed constraints model seems to be just fine
            // using structural springs.
            // Shear
            // var diagonalDist = Math.sqrt(restDistance * restDistance * 2);


            // for (v=0;v<h;v++) {
            // 	for (u=0;u<w;u++) {

            // 		constraints.push([
            // 			particles[index(u, v)],
            // 			particles[index(u+1, v+1)],
            // 			diagonalDist
            // 		]);

            // 		constraints.push([
            // 			particles[index(u+1, v)],
            // 			particles[index(u, v+1)],
            // 			diagonalDist
            // 		]);

            // 	}
            // }


            this.particles = particles;
            this.constraints = constraints;

            function index( u, v ) {

                return u + v * ( w + 1 );

            }

            this.index = index;

        }

        function simulate( time ) {

            if ( ! lastTime ) {

                lastTime = time;
                return;

            }

            var i, j, il, particles, particle, constraints, constraint;

            // Aerodynamics forces

            if ( params.enableWind ) {

                var indx;
                var normal = new Vector3();
                var indices = clothGeometry.index;
                var normals = clothGeometry.attributes.normal;

                particles = cloth.particles;

                for ( i = 0, il = indices.count; i < il; i += 3 ) {

                    for ( j = 0; j < 3; j ++ ) {

                        indx = indices.getX( i + j );
                        normal.fromBufferAttribute( normals, indx );
                        tmpForce.copy( normal ).normalize().multiplyScalar( normal.dot( windForce ) );
                        particles[ indx ].addForce( tmpForce );

                    }

                }

            }

            for ( particles = cloth.particles, i = 0, il = particles.length; i < il; i ++ ) {

                particle = particles[ i ];
                particle.addForce( gravity );

                particle.integrate( TIMESTEP_SQ );

            }

            // Start Constraints

            constraints = cloth.constraints;
            il = constraints.length;

            for ( i = 0; i < il; i ++ ) {

                constraint = constraints[ i ];
                satisfyConstraints( constraint[ 0 ], constraint[ 1 ], constraint[ 2 ] );

            }

            // Ball Constraints

            ballPosition.z = - Math.sin( Date.now() / 600 ) * 90; //+ 40;
            ballPosition.x = Math.cos( Date.now() / 400 ) * 70;

            if ( params.showBall ) {

                sphere.visible = true;

                for ( particles = cloth.particles, i = 0, il = particles.length; i < il; i ++ ) {

                    particle = particles[ i ];
                    var pos = particle.position;
                    diff.subVectors( pos, ballPosition );
                    if ( diff.length() < ballSize ) {

                        // collided
                        diff.normalize().multiplyScalar( ballSize );
                        pos.copy( ballPosition ).add( diff );

                    }

                }

            } else {

                sphere.visible = false;

            }


            // Floor Constraints

            for ( particles = cloth.particles, i = 0, il = particles.length; i < il; i ++ ) {

                particle = particles[ i ];
                pos = particle.position;
                if ( pos.y < - 250 ) {

                    pos.y = - 250;

                }

            }

            // Pin Constraints

            for ( i = 0, il = pins.length; i < il; i ++ ) {

                var xy = pins[ i ];
                var p = particles[ xy ];
                p.position.copy( p.original );
                p.previous.copy( p.original );

            }


        }

        function animate() {

            // requestAnimationFrame( animate );

            var time = Date.now();

            var windStrength = (Math.cos( time / 7000 ) * 20 + 40) / 8;

            windForce.set( Math.sin( time / 2000 ), Math.cos( time / 3000 ), Math.sin( time / 1000 ) );
            windForce.normalize();
            windForce.multiplyScalar( windStrength );

            simulate( time );
            render();

        }

        function render() {

            var p = cloth.particles;

            for ( var i = 0, il = p.length; i < il; i ++ ) {

                var v = p[ i ].position;

                clothGeometry.attributes.position.setXYZ( i, v.x, v.y, v.z );

            }

            clothGeometry.attributes.position.needsUpdate = true;

            clothGeometry.computeVertexNormals();

            sphere.position.copy( ballPosition );


        }

        return {
            animate: animate,
        }

    }
}

