import { ITEMS_CONFIG } from './view/constants';
import { Product } from './Product';

export class SilentLoader{

    public colorProperties: string[];
    public textProperties: string[];

    private product: Product;
    private productConfig: any;
    

    constructor(){

        this.product = new Product();
        this.getProductColors();
    }

    /*
        IMPORTANT
            - dont break anything that exists already, if you need to modify existing function, better clone it in this class

        TODO - flow of how things should be implemented

        1st PART
        - load product specific textures, skip normal maps tho (Main.ts). Texture loader example at ProductSweater or ProductGarmet - loadTexture()
        - create temporary canvases from loaded textures
        - in PNG2BMPConverter.ts use function getBMPFileFromImageData() as an example and create a shorter version of it here. 
        - Pass a texture (as canvas) to getBMPFileFromImageData() and generate array of _originalMaskColors
        - in ColorControls.ts use function detectActiveColors() as an example to detect active colors on different elements (that are allowed to edit by config)
        - store detected colors into colorProperties and make them accessible from product.liquid (shopify)
        - create dynamic input elements (one hidden input for color hex code and one dynamic input for color name) using colorProperties values. In product.liquid I've already set up how it should look - search for 'custom-sweater'
        - some products can  have custom text, so just check productConfig.customText, create hidden inputs for text color, text position (frt - front, bck - back, slv - sleave, etc)

    */
    private getProductColors(){

        this.productConfig = this.product.getItemConfig();

    }
}

/*
    2nd PART - customizing colors
    - when color is changed, modify hidden inputs created in 1st part
    - need to follow name/id in same manner
    - 

    3rd PART - add to cart
    - when product added to cart - trigger only screenshot feature
    - actually need to display image as a custom property, this way we can replace main image?
    - 

    4th PART - webhooks


*/