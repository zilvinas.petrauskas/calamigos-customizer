import { ColorControls } from './ColorControls';

// import {
// 	Mapping,
// 	Wrapping,
// 	TextureFilter,
// 	PixelFormat,
// 	TextureDataType,
// } from '../constants';


// this class holds colors information for different products
export class ColorsConfig{

    private static config = {}

    constructor(){

        const item = {};

        // TODO - make a config for which parts gets colored, when specific color is clicked (base, imprint, etc...)

        ColorsConfig.config = item;
    }

    public static get(productID: string){

        if(ColorsConfig.config.hasOwnProperty(productID)){
            return ColorsConfig.config[productID];
        }
        return [];
    }

    

}