export class PerformanceCheck{
    
    private start;
    private end;
    private task = '';
    
    constructor(taskName){

        this.task = taskName;
        this.start = performance.now();
    }

    public stop(){
        this.end = performance.now();
        const diff = this.end - this.start;
        console.log(`~ PERFORMANCE ${this.task} took ${diff}ms`);
    }
}