import { SceneManager } from './SceneManager';
import { Color, Scene, AmbientLight, DirectionalLight, Fog, RepeatWrapping, MeshLambertMaterial, Mesh, PlaneBufferGeometry, BoxBufferGeometry, Math as THREEMATH, Vector3, PointLight, HemisphereLight } from 'three';
import { Main } from './../Main';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';

export class GrassEnviroment{

    private _scene: SceneManager;
    private _world: Scene;
    private _main: Main;
    private _textures: any;
    public poleConfig: any;
    public blanketConfig: any;
    public groundConfig: any;
    public addFlowers: boolean;
    public lightConfig: any;
    private flowers: any;

    constructor(scene: SceneManager, main: Main, _textures){

        this._main = main;
        this._scene = scene;
        this._world = this._scene.scene;
        this._textures = _textures;

        this.addFlowers = false;

        this.poleConfig = {
            w: 270,
            h: 447.5,
            y: 225, // for horizontal pole
        }

        this.blanketConfig = {
            y: 36, // just a fix, not real y
        }

        this.groundConfig = {
            // y: -(this.poleConfig.h/2 - 5),
            y: -220,
        }

        this.lightConfig = {
            directional : {
                position: new Vector3(50, 200, 100),
            }
        }

       

        //
    }

    

    public init(){

        // fog
        this._world.fog = new Fog( 0xccecff, 500, 10000 );

        this._world.background = new Color (0xccecff);

        // lights
        this._world.add( new AmbientLight( 0xffffff, 0.2 ) );

        //var light = new DirectionalLight( 0xffffff, 1 );

        //light.position.copy(this.lightConfig.directional.position); //set( 50, 200light.position., 100 );
        // light.position.set(50, 200, 100);
        // light.position.multiplyScalar( 1.3 );

        // var d = 300;
        // light.castShadow = true;
        // light.shadow.mapSize.width = 1024;
        // light.shadow.mapSize.height = 1024;
        // light.shadow.camera.left = - d;
        // light.shadow.camera.right = d;
        // light.shadow.camera.top = d;
        // light.shadow.camera.bottom = - d;
        // light.shadow.camera.far = 1000;

        // this._world.add( light );


        // New lights

        var d = 150;

        var light2 = new PointLight( 0xdfebff, 1.5 );
        light2.position.set(25, 500, 500);
        light2.position.multiplyScalar( 1 );
        light2.castShadow = true;
        light2.shadow.bias = 0.00002;
        light2.shadow.radius = 25;
        light2.shadow.mapSize.width = 1024;
        light2.shadow.mapSize.height = 1024;
        light2.shadow.camera.far = 10000;

        var light3 = new PointLight( 0xdfebff, 1.5 );
        light3.position.set(-25, 500, -500);
        light3.position.multiplyScalar( 1 );
        light3.castShadow = false;
        light3.shadow.bias = 0.0002;
        light3.shadow.radius = 25;
        light3.shadow.mapSize.width = 1024;
        light3.shadow.mapSize.height = 1024;
        light3.shadow.camera.far = 100;
        
        this._world.add( light2 );
        this._world.add( light3 );

        // var hemiLight = new HemisphereLight(0x000000, 0xffffff, 1);
        // hemiLight.position.set(1, 1, 1);
        // this._world.add(hemiLight);


         // ground

         const groundTexture = this._textures.GRASS;
         groundTexture.wrapS = groundTexture.wrapT = RepeatWrapping;
         groundTexture.repeat.set( 25, 25 );
         groundTexture.anisotropy = 16;
 
         const groundMaterial = new MeshLambertMaterial( { map: groundTexture } );
 
         const groundMesh = new Mesh( new PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
         groundMesh.position.y = this.groundConfig.y;
         groundMesh.rotation.x = - Math.PI / 2;
         groundMesh.receiveShadow = true;
         this._world.add( groundMesh );
 
         // poles
 
         const poleGeo = new BoxBufferGeometry( 5, this.poleConfig.h, 5 );
         const poleMat = new MeshLambertMaterial({
             map: this._textures.WOOD,
         });
 
         const leftPole = new Mesh( poleGeo, poleMat );
         leftPole.position.x = - this.poleConfig.w/2;
         leftPole.position.y = this.groundConfig.y + this.poleConfig.h / 2; //0; //-this.poleConfig.h / 2;
         leftPole.receiveShadow = true;
         leftPole.castShadow = true;
         this._world.add( leftPole );
 
         const rightPole = new Mesh( poleGeo, poleMat );
         rightPole.position.x = this.poleConfig.w/2;
         rightPole.position.y = this.groundConfig.y + this.poleConfig.h / 2; //0; //-this.poleConfig.h / 2;
         rightPole.receiveShadow = true;
         rightPole.castShadow = true;
         this._world.add( rightPole );
 
         const horizontalPole = new Mesh( new BoxBufferGeometry( this.poleConfig.w, 5, 5 ), poleMat );
         horizontalPole.position.x = 0;
         horizontalPole.position.y = this.poleConfig.y; // this.groundConfig.y - 7.5 + this.poleConfig.h;
        //  console.log('pole horizontal y', horizontalPole.position.y);
         horizontalPole.receiveShadow = true;
         horizontalPole.castShadow = true;
         this._world.add( horizontalPole );
 
         const poleBottom = new BoxBufferGeometry( 10, 10, 10 );
 
         const poleBottomRight = new Mesh( poleBottom, poleMat );
         poleBottomRight.position.x = this.poleConfig.w/2;
         poleBottomRight.position.y = this.groundConfig.y;
         poleBottomRight.receiveShadow = true;
         poleBottomRight.castShadow = true;
         this._world.add( poleBottomRight );
 
         const poleBottomLeft = new Mesh( poleBottom, poleMat );
         poleBottomLeft.position.x = - this.poleConfig.w/2;
         poleBottomLeft.position.y = this.groundConfig.y;
         poleBottomLeft.receiveShadow = true;
         poleBottomLeft.castShadow = true;
         this._world.add( poleBottomLeft );


         if(this.addFlowers){
            this.loadFlowers().catch(this.failedLoadingFlowers);
         }

    }


    private async loadFlowers(){

        this.flowers = {
            model: await this.loadFlowerModel(),
        }

        this.createFlowers();
    }

    private failedLoadingFlowers(err){
        console.log('load texture failed', err);
    }

    private async loadFlowerModel(){

        let flowerDir = this._main.getBaseUrl();

        // const progressBar = this.progressBar;
    
        let _baseURL = this._main.getBaseUrl();
        let url = _baseURL +'dandelions.glb';

        // progressBar.set(50);

        // load it in glb - faster
        return new Promise<any>((resolve, reject) =>{
            const gltfLoader = new GLTFLoader();			

            gltfLoader.load(url, (gltf: GLTF) =>{
               
                const object = gltf.scene;
                // progressBar.set(75);
                resolve(object);

            }, this.onProgress);
        });


    }

    private onProgress = (progressEvent: ProgressEvent) =>{

		if (progressEvent.lengthComputable){
			// this.progressBar.update(progressEvent.loaded,progressEvent.total)
		}

    };
    
    private createFlowers(){

        
        // flowers
        if(this.addFlowers){

            console.log('got flower model?', this.flowers.model);

            const flowersData = [
                {
                    x: 398, 
                    y: this.groundConfig.y, 
                    z: -425,
                    rotation: 0, // degrees
                },
                {
                    x: -396, 
                    y: this.groundConfig.y, 
                    z: -337,
                    rotation: 50, // degrees
                },
                {
                    x: 464, 
                    y: this.groundConfig.y, 
                    z: -1219,
                    rotation: 140, // degrees
                },
                {
                    x: -462, 
                    y: this.groundConfig.y, 
                    z: -1100,
                    rotation: 230, // degrees
                },
            ];


            const radiuses = [400, 700, 1100];
            const amounts = [15, 25, 40];

            // const amount = 20;
            // const circleRadius = 500; // 600
            const randomness = 50;
            const center = new Vector3(0, this.groundConfig.y, 0);
            
            let i = 0;

            const flowerModels = [];

            for(let r in radiuses){

                let currentAngle = 0;
                let radius = radiuses[r];
                let amount = amounts[r];
                let angleChange = 2 * Math.PI / amount;

                for(let a=0;a<amount;a++){

                    flowerModels[i] = this.flowers.model.clone();
                    let randomScale = Math.random() * 150 + 200;
                    flowerModels[i].scale.set(randomScale, randomScale, randomScale);

                    let randomX = Math.round(Math.random()) * randomness - (randomness/2);
                    let randomZ = Math.round(Math.random()) * randomness - (randomness/2);

                    let randomPosition = new Vector3(
                        radius * Math.cos(currentAngle) + randomX,
                        0,
                        radius * Math.sin(currentAngle) + randomZ
                    ).addScaledVector(center, 1);

                    flowerModels[i].position.copy(randomPosition);

                    let randomRotation = Math.random() * 360;
                    flowerModels[i].rotation.y = THREEMATH.degToRad(randomRotation);

                    this._world.add(flowerModels[i]);

                    currentAngle += angleChange;
                    i++;
                }

            }

           
            // for(let i=0;i<amount;i++){
                
            //     flowerModels[i] = this.flowers.model.clone();

            //     let randomScale = Math.random() * 150 + 300; // 350 to 450
            //     flowerModels[i].scale.set(randomScale, randomScale, randomScale);

            //     let randomX = Math.round(Math.random()) * randomness - (randomness/2);
            //     let randomZ = Math.round(Math.random()) * randomness - (randomness/2);

            //     let randomPosition = new Vector3(
            //         circleRadius * Math.cos(currentAngle) + randomX,
            //         0,
            //         circleRadius * Math.sin(currentAngle) + randomZ
            //     ).addScaledVector(center, 1);
            //     flowerModels[i].position.copy(randomPosition);

            //     let randomRotation = Math.random() * 360;
            //     flowerModels[i].rotation.y = THREEMATH.degToRad(randomRotation);

            //     this._world.add(flowerModels[i]);

            //     currentAngle += angleChange;
            // }


            // let gui = null;

            // // flower is too small in its original scale
            // const s = 400; // scale it up, default 400
            // this.flowers.model.scale.set(s,s,s);

            // const flowerModels = [];
            // for(let i in flowersData){
            //     flowerModels[i] = this.flowers.model.clone();
            //     flowerModels[i].position.set(flowersData[i].x, flowersData[i].y, flowersData[i].z);
            //     flowerModels[i].rotation.y = THREEMATH.degToRad(flowersData[i].rotation);

            //     this._world.add(flowerModels[i]);
            //     console.log('flower #'+i, flowerModels[i].position);

            //     // gui controls only on localhost
            //     if (window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1"){

            //         if(gui === null)  gui = new dat.GUI();

            //         let flowerGui = gui.addFolder('Flower #'+(parseInt(i)+1)); 
            //         flowerGui.add(flowerModels[i].position, 'x', -1000, 1000).name('Pos X').step(1).onChange((val)=>{
            //             flowerModels[i].position.x = val;
            //         });
            //         flowerGui.add(flowerModels[i].position, 'z', -2000, 0).name('Pos Z').step(1).onChange((val)=>{
            //             flowerModels[i].position.z = val;
            //         });
            //         flowerGui.add(flowerModels[i].rotation, 'y', -Math.PI, Math.PI).name('Rotation').step(0.1).onChange((val)=>{
            //             flowerModels[i].rotation.y = val;
            //             console.log('Degrees #'+i, THREEMATH.radToDeg(val));
            //         });
            //     }
            // }
            
        }

    }


}