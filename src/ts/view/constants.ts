
export const EXTERNAL_SERVER_URL = 'https://calamigos.zilvinas.pro/customizer.php'; // full link, with customizer.php at the end or whatever file is named

// gradient background colors
export const BACKGROUND_COLOR_START = 'ffffff';
export const BACKGROUND_COLOR_END = 'aeaeae';
// export const BACKGROUND_COLOR_END = '343434';

/* name of 3d models */ 
// export const SWEATER_3D_MODEL = 'shirt_v19.glb'; 
export const SWEATER_3D_MODEL = 'new_shirt_v9.glb'; 
// export const BLANKET_3D_MODEL = 'blanket_v2.glb'; // not used currently, creating blanket manually in code

export const MODEL_FRONT = 'FRT';
export const MODEL_BACK = 'BCK';
export const MODEL_SLEEVE = 'SLV';
export const MODEL_COLLAR = 'CLR'; // apykakle
export const MODEL_RIB = 'RIB'; // juosta apacioje
export const MODEL_CUFF = 'CUF';  // rankogalis

export const COLOR_BLACK_SWAN = '090a07'; // black
export const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
export const COLOR_MARINA_BLUE = '262847'; // darkish blue
export const COLOR_CLASSIC_CAMEL = '9f7d5e'; // brown
export const COLOR_ORANGE = 'df6536'; // orange
export const COLOR_FINE_FLANNEL = '737474'; // gray
export const COLOR_RED_HOT = '9f1c28'; // red
export const COLOR_PINE_FOREST = '173321'; // forest green

export const COLOR_NAMES = {};
COLOR_NAMES[COLOR_BLACK_SWAN] = 'Black Swan';
COLOR_NAMES[COLOR_WHITE_COTTON] = 'White Cotton';
COLOR_NAMES[COLOR_MARINA_BLUE] = 'Marina Blue';
COLOR_NAMES[COLOR_CLASSIC_CAMEL] = 'Camel Brown';
COLOR_NAMES[COLOR_ORANGE] = 'Orange';
COLOR_NAMES[COLOR_FINE_FLANNEL] = 'Grey Flannel';
COLOR_NAMES[COLOR_RED_HOT] = 'Red Hot';
COLOR_NAMES[COLOR_PINE_FOREST] = 'Forest Green';

// just for test

export const AO_MAP_INTENSITY = 1.8;

export const ALL_COLORS = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST
];

export const SWEATER_CONFIG = {
    envMap: 'environment.jpg',
    aoMap: 'ao3.png', // base
    ribMap: 'ao3_rib_cuf.png', // rif + cuf
    collarMap: 'ao3_collar.png', // collar
}

// make sure key here is always uppercased
export const ITEMS_CONFIG = {
    
    'CC001': {
        names: {
            ribcuff: 'Rib & Cuff Color',
        },
        masks: {
            // base: ['F0', 'B0', 'SLV0',  ],
            // bottom: ['F2', 'B1', 'B2', 'SLV1', 'SLV2'],
            // collar: ['CLR0'],
            ribcuff: ['RIB0', 'CUF0'], 
        },
        colors: {
            ribcuff: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_WHITE_COTTON,
            CUF: COLOR_RED_HOT,
            RIB: COLOR_RED_HOT,
        }
    },

    'CC002': {
        names: {
            base: 'Body Color',
        },
        masks: {
            base: ['F0', 'B0', 'SLV0', 'CLR0', 'RIB0', 'CUF0'],
            // stripe1: ['F2', 'B2', 'SLV2'],
            // stripe2: ['F3', 'B3', 'SLV3'],
            // stripe3: ['F4', 'B4', 'SLV4'],
            // stripe4: ['F5', 'B5', 'SLV5'],
        },
        colors: {
            base: [COLOR_WHITE_COTTON, COLOR_FINE_FLANNEL],
        },
        predefined: {
            CLR: COLOR_WHITE_COTTON,
            CUF: COLOR_WHITE_COTTON,
            RIB: COLOR_WHITE_COTTON,
        },
        customText: [
            {
                position: 'SLV',
                limit: 2,
                color: COLOR_PINE_FOREST,
            }
        ],
        
        
    },
  
    'CC003': {
        names: {
            base: 'Body Color',
        },
        masks: {
            base: ['F0', 'B0', 'SLV0', 'CLR0', 'RIB0', 'CUF0'],
        },
        colors: {
            base: [COLOR_WHITE_COTTON, COLOR_FINE_FLANNEL],
        },
        predefined: {
            CLR: COLOR_FINE_FLANNEL,
            CUF: COLOR_FINE_FLANNEL,
            RIB: COLOR_FINE_FLANNEL,
        },
    },

    'CC004': {
        names: {
            base: 'Body Color',
            topStripe: 'Top Stripe Color',
            bottomStripe: 'Bottom Stripe Color',
        },
        masks: {
            base: ['F0', 'B0', 'SLV0', 'RIB0', 'CLR0', 'CUF0'],
            topStripe: ['SLV1'],
            bottomStripe: ['SLV2'],
        },
        colors: {
            base: [...ALL_COLORS],
            topStripe: [...ALL_COLORS],
            bottomStripe: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_PINE_FOREST,
            CUF: COLOR_PINE_FOREST,
            RIB: COLOR_PINE_FOREST,
        },
    },

    'CC005': {
        names: {
            base: 'Body Color',
            topPinstripe: 'Top Pinstripe',
            bottomPinstripe: 'Bottom Pinstripe',
            collar: 'Collar Color',
        },
        masks: {
            base: ['F0', 'B0', 'SLV0',  'RIB0', 'CUF0'],
            topPinstripe: ['F2', 'B2', 'SLV1'],
            bottomPinstripe: ['F1', 'B1', 'SLV2'], 
            collar: ['CLR0'],
        },
        colors: {
            base: [...ALL_COLORS],
            topPinstripe: [...ALL_COLORS],
            bottomPinstripe: [...ALL_COLORS],
            collar: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_CLASSIC_CAMEL,
            CUF: COLOR_CLASSIC_CAMEL,
            RIB: COLOR_CLASSIC_CAMEL,
        },
    },
    
    'CC006': {
        names: {
            base: 'Body Color',
            topPinstripe: 'Top Pinstripe',
            bottomPinstripe: 'Bottom Pinstripe',
            collar: 'Collar Color',
        },
        masks: {
            base: ['F0', 'B0', 'SLV0', 'RIB0', 'CUF0'],
            topPinstripe: ['F1', 'B1', 'SLV2'],
            bottomPinstripe: ['F2', 'B2', 'SLV1'], 
            collar: ['CLR0'],
        },
        colors: {
            base: [...ALL_COLORS],
            topPinstripe: [...ALL_COLORS],
            bottomPinstripe: [...ALL_COLORS],
            collar: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_ORANGE,
            CUF: COLOR_PINE_FOREST,
            RIB: COLOR_PINE_FOREST,
        },
    },

    'CC007': {
        names: {
            collar: 'Collar Color',
            ribcuff: 'Rib & Cuff Color',
            text: 'Text Color',
        },
        masks: {
            collar: ['CLR0'],
            ribcuff: ['RIB0', 'CUF0'],
            text: ['F3', 'B3','FRT_TEXT'],
        },
        colors: {
            collar: [...ALL_COLORS],
            ribcuff: [...ALL_COLORS],
            text: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_RED_HOT,
            CUF: COLOR_PINE_FOREST,
            RIB: COLOR_PINE_FOREST,
        },
        customText: [
            {
                position: 'FRT',
                color: COLOR_ORANGE,
            }
        ],
    },

    'CC008': {
        names: {            
            collar: 'Collar Color',
            ribcuff: 'Rib & Cuff Color',
            // text: 'Text Color',
        },
        masks: {
            collar: ['CLR0'],
            ribcuff: ['RIB0', 'CUF0'],
            // text: ['F4','B4','FRT_TEXT'],
        },
        colors: {
            collar: [...ALL_COLORS],
            ribcuff: [...ALL_COLORS],
            // text: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_RED_HOT,
            CUF: COLOR_MARINA_BLUE,
            RIB: COLOR_MARINA_BLUE,
        },
        customText: [
            {
                position: 'FRT',
                color: COLOR_RED_HOT,
            }
        ],
    },

    'CC009': {

        names: {
            collar: 'Collar Color',
            ribcuff: 'Rib & Cuff Color',
            text: 'Text Color',
        },
        masks: {
            collar: ['CLR0'],
            ribcuff: ['RIB0', 'CUF0'],
            text: ['F5','B4','FRT_TEXT'],
        },
        colors: {
            collar: [...ALL_COLORS],
            ribcuff: [...ALL_COLORS],
            text: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_MARINA_BLUE,
            CUF: COLOR_MARINA_BLUE,
            RIB: COLOR_MARINA_BLUE,
        },
        customText: [
            {
                position: 'FRT',
                color: COLOR_MARINA_BLUE,
            }
        ],
    },

    'CC010': { 
        names: {
            base: 'Body Color',
            rope: 'Rope Color',
        },
        masks: {
            base: ['F0','F2', 'B0', 'SLV0','SLV3', 'CLR0', 'RIB0', 'CUF0'], // F2 is actual front (keeping F0 in case texture is); SLV3 for oval part within rope on sleeves
            rope: ['F1', 'B1', 'SLV1', 'SLV2'], // slv1 - on body, slv2 - on sides
        },
        colors: {
            base: [...ALL_COLORS],
            rope: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_BLACK_SWAN,
            CUF: COLOR_BLACK_SWAN,
            RIB: COLOR_BLACK_SWAN,
        },
        customText: [
            {
                position: 'F0',
                color: COLOR_WHITE_COTTON
            }
        ],
    },

    'CC011': {
        names: {
            base: 'Body Color',
            logo: 'Logo Color',
            collar: 'Collar Color',
        },
        masks: {
            base: ['F0', 'B0', 'SLV0','RIB0','CUF0'],
            logo: ['F1'],
            collar: ['CLR0'],
        },
        colors: {
            base: [...ALL_COLORS],
            logo: [...ALL_COLORS],
            collar: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_MARINA_BLUE,
            CUF: COLOR_MARINA_BLUE,
            RIB: COLOR_MARINA_BLUE,
        },
    },

    'CC013': {
        names: {
            base : 'Body Color',
            topPinstripe: 'Top Pinstripe',
            bottomPinstripe: 'Bottom Pinstripe',
        },
        masks: {
            base : ['F0','B0','SLV0', 'RIB0','CUF0','CLR0'], 
            topPinstripe: ['F2','B2','F3'], 
            bottomPinstripe: ['F1','B1'], 
        },
        colors: {
            base: [...ALL_COLORS], 
            topPinstripe: [...ALL_COLORS], 
            bottomPinstripe: [...ALL_COLORS], 
        },
        predefined: {
            CLR: COLOR_FINE_FLANNEL,
            CUF: COLOR_FINE_FLANNEL,
            RIB: COLOR_FINE_FLANNEL,
        },
    },

    'CC014': {
        names: {
            base: 'Body Color',
            topPinstripe: 'Top Pinstripe',
            bottomPinstripe: 'Bottom Pinstripe',
            ribcuff: 'Rib & Cuff Color',
        },
        masks: {
            base: ['F0', 'B0', 'SLV0','CLR0'],
            topPinstripe: ['F2', 'B2'],
            bottomPinstripe: ['F1', 'B1'],
            ribcuff: ['RIB0', 'CUF0'],
        },
        colors: {
            base: [...ALL_COLORS],
            topPinstripe: [...ALL_COLORS],
            bottomPinstripe: [...ALL_COLORS],
            ribcuff: [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_MARINA_BLUE,
            CUF: COLOR_MARINA_BLUE,
            RIB: COLOR_MARINA_BLUE,
        },
    },

    // garmet
    'CCB001': {
        names: {
            base : 'Top Stripe',
            stripe: 'Bottom Stripe',
            arrowTop: 'Top Arrow',
            arrowBottom: 'Bottom Arrow',
        },
        masks: {
            base : ['BLANKET0'],
            stripe: ['BLANKET2'],
            arrowTop: ['BLANKET1'],
            arrowBottom: ['BLANKET3'],
           
        },
        colors: {
            base : [...ALL_COLORS],
            stripe: [...ALL_COLORS],
            arrowTop: [...ALL_COLORS],
            arrowBottom: [...ALL_COLORS], 
        },
        customText: [
            {
                position: 'BLANKET_TOP',
                y: 95,
                limit: 20,
                color: COLOR_PINE_FOREST,
                follow: 'BLANKET3'
            },
            {
                position: 'BLANKET_BOT',
                y: 95,
                limit: 20,
                color: COLOR_ORANGE,
                follow: 'BLANKET0'
            }
        ],
        normalMap: {
            BLANKET: 'M0001_CCB001_normal.png',
        },
    },

    // garmet
    'CCB002': {
        names: {
            base: 'Body Color',
            stripeTop: 'Top Stripe',
            stripeBottom: 'Bottom Stripe',
        },
        masks: {
            base: ['BLANKET1'],
            stripeTop: ['BLANKET0'],
            stripeBottom: ['BLANKET2'],
        },
        colors: {
            base: [...ALL_COLORS],
            stripeTop: [...ALL_COLORS],
            stripeBottom: [...ALL_COLORS],
        },
        customText: [
            {
                position: 'BLANKET_TOP',
                limit: 20,
                color: COLOR_ORANGE,
                follow: 'BLANKET0', // above is default color, but if BLANKET0 is changed, text color will follow
            },
            {
                position: 'BLANKET_BOT',
                limit: 20,
                color: COLOR_ORANGE,
                follow: 'BLANKET0',
            }
        ],
        normalMap: {
            BLANKET: 'M0001_CCB002_normal.png',
        },
    
    },

    // garmet
    'CCB004': {
        names: {
            edge: 'Edge Color',
            base: 'Body Color', 
            text: 'Text Color',
        },
        masks: {
            edge: ['BLANKET1'],
            base: ['BLANKET2'],
            text: ['BLANKET3'],
        },
        colors: {
            edge: [...ALL_COLORS],
            base: [...ALL_COLORS],
            text: [...ALL_COLORS],
        },
        normalMap: {
            BLANKET: 'M0001_CCB004_normal.png',
        },
    },


}


// locally need these values to display stuff
// CC001, CC003, CC007, CC008, CC010, CC011, CCB002, CCB004

// [Z] - renamed all the masks and removed M0001
export const LOCALHOST_FALLBACK = {
    PRODUCT_ID: 'CC006',
    FRT : "../localhost/CC006_F.png",
	BCK: "../localhost/CC006_B.png",
    SLV: "../localhost/CC006_SLV.png",
    baseURL : "",
    BLANKET: '../localhost/CCB001.png',
}