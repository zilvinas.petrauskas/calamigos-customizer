/*

const parseIMGURL = function(_section, isMask){ // for normal maps isMask should be false
    const formatType = `png`;
    const href = location.href.split(`/`);
    try{
      let section = _section;
      if(typeof isMask === 'undefined' || isMask === null) isMask = true;
      if( ! isMask) section = _section.split('_')[0]; // for normal maps its F_NORMAL, so for section only use 'F' 
      
      const lastPart = href[href.length - 1];
      const splittedLastPart = lastPart.split(`_`);
      const productType = splittedLastPart[0].toUpperCase();
      const productID = splittedLastPart[1].split('?')[0].split('-')[0].toUpperCase();
      if(PRODUCT_ID === null && productID.search('CC0') > -1) PRODUCT_ID = productID;
      const assetURL = "{{ "./" | asset_url }}";
      baseURL = assetURL.split('./?')[0];
      
      if(CUSTOM_MASKS && CUSTOM_MASKS[productID] && CUSTOM_MASKS[productID][_section] ){
        const customFileName = CUSTOM_MASKS[productID][_section];
        return [baseURL, customFileName];
        //return `${baseURL}${customMask}`; 
      }

      let fileName = `${productType}_${productID}_${section}.${formatType}`; // masks / textures
      if( ! isMask) `${productType}_${productID}_${section}_normal.${formatType}`; // normal maps
      
      return [baseURL, fileName];
      //return `${baseURL}${productType}_${productID}_${section}.${formatType}`;
    }catch(e){
     	return ''; // section not found 
    }
  }
  const parseBlanketIMGURL = function(section, isMask){
        const formatType = `png`;
        const href = location.href.split(`/`);
        if(typeof isMask === 'undefined' || isMask === null) isMask = true;
        const productID = href[href.length - 1].toUpperCase();
        if(PRODUCT_ID === null && productID.search('CCB') > -1) PRODUCT_ID = productID;
        const assetURL = "{{ "./" | asset_url }}";
        baseURL = assetURL.split('./?')[0];
        //return `${baseURL}${productID}.${formatType}`;
    
      	if(CUSTOM_MASKS && CUSTOM_MASKS[productID] && CUSTOM_MASKS[productID][section]){
          const customFileName = CUSTOM_MASKS[productID][section];
          return [baseURL, customFileName];
          //return `${baseURL}${customMask}`; 
        }
    	let fileName = `${productID}.${formatType}`;
    	if(!isMask) fileName = `${productID}_normal.${formatType}`;
    
    	return [baseURL, fileName];
  }
  
  const getSweaterMask = function(section){
     const [baseURL, fileName] = parseIMGURL(section);
     return `${baseURL}${fileName}`;
  }
  
  const getBlanketMask = function(){
   	 const [baseURL, fileName] = parseBlanketIMGURL('BLANKET');
     return `${baseURL}${fileName}`;
  }
  
  const getSweaterNormal = function(section){
    const [baseURL, fileName] = parseIMGURL(`${section}_NORMAL`, false); // make sure its F_NORMAL, B_NORMAL or SLV_NORMAL, not F, B, SLV
    return `${baseURL}${fileName}`;
  }
  
  const getBlanketNormal = function(){
    const [baseURL, fileName] = parseBlanketIMGURL('BLANKET_NORMAL', false);
     return `${baseURL}${fileName}`;
  }
  
  
    let PRODUCT_ID = null; // will be set in getIMGURL fn
   
  	const imgURL_FRT = getSweaterMask(`F`); // Front
  	const imgURL_BCK = getSweaterMask(`B`); // Back
  	const imgURL_SLV = getSweaterMask(`SLV`); // SLV
    const imgURL_BLANKET = getBlanketMask(); // blanket
  
  	const NORMAL_FRT = getSweaterNormal(`F`);
  	const NORMAL_BCK = getSweaterNormal(`B`);
  	const NORMAL_SLV = getSweaterNormal(`SLV`);
  	const NORMAL_BLANKET = getBlanketNormal();
 

  */