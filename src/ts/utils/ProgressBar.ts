import { SceneManager } from "./../view/SceneManager";

export class ProgressBar{

	private currentProgress = 0;
	private delayedProgress = 0;
	private $currentProgress = document.getElementById('currentProgress');
	private $canvasWrapper = document.getElementById('canvasWrapper');
	private $progressBarElement;
	private _scene: SceneManager;
	private done = false;
	private loop:any;
	private showCustomizerOnLoad: boolean = false;

	/*
		a) if customizer finished loading assets - show it without progress bar
		b) if customizer still loading, show progress bar first

		by default it should show progress bar
		
	*/

	constructor(scene: SceneManager){
		this._scene = scene;
	}

	public makeVisible(){
		document.getElementById('customizerProgressBar').classList.add('loading');
		console.log('progress bar visible', this.done, this.currentProgress);
	}

	public finished(){
		return this.done;
	}

	public startAnimationLoop(){
		this.loop = requestAnimationFrame(()=>{
			this.animate(true);
		});
	}

    public update = (loaded, total) =>{

		let percent = loaded / total;
		const min = 10; // minimum is 10% loaded, so that user sees progress instantly
		if(percent < min) percent = min;
		if(percent > this.delayedProgress){
			this.delayedProgress = percent;
		}	

	};

	public set(progress){
		if(progress > this.delayedProgress && progress > 0 && progress <= 100){
			this.delayedProgress = progress;
		}
	}
	

	private animate = (loop: boolean = false)=>{

		if( ! this.done){
			if(this.currentProgress < this.delayedProgress){
				this.currentProgress += 1;
				this.currentProgress = Math.min(100, this.currentProgress); // cap at 100
				this.$currentProgress.style.width = this.currentProgress+'%';
				// console.log('progress', this.currentProgress);
			}else if(this.currentProgress >= 100){
				this.done = true;
				console.log('progress bar - DONE');
				this.onComplete();
			}
			if(loop===true){
				this.loop = requestAnimationFrame(()=>{
					this.animate(true);
				});
			}
		}

		return this.done;
	}

	public finish(){
		this.delayedProgress = 100;
		this.animate();
	}

	public onComplete(){

		console.log('- progress bar - onComplete', this.showCustomizerOnLoad);

		cancelAnimationFrame(this.loop);
		document.getElementById('customizerProgressBar').classList.add('hide-progress');
		if(this.onCompleteCallback) this.onCompleteCallback();
		if(this.showCustomizerOnLoad) this._scene.controlsVisibilityToggle(true);
	}

	// this should only be called if silent loader is ON
	public queueAutoShow(){
		console.log('- queue customizer visibility change');
		this.showCustomizerOnLoad = true;
	}

	public onCompleteCallback(){
		// set this function from outside to trigger extra stuff on load complete
	}
}