/** This is a (kindof) Singleton class. The purpose of this is to handle AJAX requests */

export interface AJAXConfig
{
	url: string;
	params?: any;
	method: string;
	onComplete: (response: string) => any;
	onFail?: (status: number, statusText: string) => any;
	async?: boolean;
	xhrFields?: any,
}

export class AJAX
{
	public static BASE_URL = "localhost";
	public static METHOD_GET  = "GET";
	public static METHOD_POST = "POST";

	public static load = (config: AJAXConfig) =>
	{
		/** Adding default values to the optionals, if there was none given */
		if (config.async == null)
		{
			config.async = true;
		}
		if (config.onFail == null)
		{
			config.onFail = AJAX.defaultOnFail;
		}
		if (config.method == null)
		{
			config.method = AJAX.METHOD_POST;
		}

		let url = config.url;

		if (config.params && config.method === AJAX.METHOD_GET)
		{
			url += "?" + AJAX.encodeParams(config.params);
		}
		const xhr = new XMLHttpRequest();

		xhr.onreadystatechange = () =>
		{
			if (xhr.readyState === 4 && xhr.status === 200)
			{
				config.onComplete(xhr.responseText);
			}
			else if (xhr.readyState === 4 && xhr.status !== 200)
			{
				config.onFail(xhr.status, xhr.statusText);
			}
		};

		xhr.onerror = xhr.ontimeout = xhr.onabort = () =>
		{
			config.onFail(xhr.status, xhr.statusText);
		};

		xhr.open(config.method, url, config.async);

		if (config.method === AJAX.METHOD_POST)
		{
			const encoded_params = AJAX.encodeJSONParams(config.params);

			xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
			xhr.send(encoded_params);
		}
		else
		{
			xhr.send();
		}
	}

	private static defaultOnFail = (status: number, statusText: string) =>
	{
		console.warn('AJAX call failed!');
		console.warn('Status: ' + status + ' (' + statusText + ')');
	};

	private static encodeParams(params: any): string
	{
		const str = [];

		for (let key in params)
		{
			str.push(encodeURIComponent(key) + "=" + encodeURIComponent(params[key]));
		}
		return str.join("&");
	}

	private static encodeJSONParams = (params: any): string =>
	{
		const str = JSON.stringify(params);

		return str;
	}
}