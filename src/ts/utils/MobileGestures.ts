
export class MobileGestures{
 
    // supports swipe down action on mobile
    constructor(optionsWrapper, callback){

        const swipe = optionsWrapper;

        let touchstartY = 0;
        let touchendY = 0;
        const minDiffY = 50; // min distance in pixels for swipe down to trigger

        let optionsContainer = optionsWrapper.querySelectorAll('#optionsContainer')[0];
      

        // optionsContainer.addEventListener('scroll', (e)=>{
        //     onScrollDown(e);
        // })
        
        swipe.addEventListener('touchstart', function(event) {
          touchstartY = event.changedTouches[0].screenY;
        }, false);
      
        swipe.addEventListener('touchend', function(event) {
          touchendY = event.changedTouches[0].screenY;
            handleSwipe();
        }, false); 
      
        function handleSwipe() {
            if (touchendY > touchstartY + minDiffY) {
                if(callback) callback();
            }
        }

        let delayed = null;

        function onScrollDown(e){
            clearTimeout(delayed);
            delayed = setTimeout(()=>{

                if(optionsContainer.scrollTop === 0){
                    handleSwipe(); // only trigger swipe down action when container is scroleld at top
                }

            }, 30);

        }


    }    

}