<?php

require_once 'PDF.php';
require_once 'Product.php';



class PackingSlip{

    // free to change
    public $customerName = "Garrett Gerson";
    public $address1 = "327 Latigo Canyon Rd";
    public $address2 = "Malibu, CA 90265";
    public $address3 = "";

    public $orderNumber = "24573";
    public $orderDate = null; // left null, code will write current day
    public $shippingMethod = 'Free shipping';

    // private
    private $pdf;
    private $marginLeft = 20; // margin left / right
    private $currentY = 0;
    private $list = [];

    private $fontSize = 14;
    private $fontFamily = 'Arial';

    public function __construct($productList=[]){

        $this->pdf = new PDF();
        $this->list = $productList;

    }

    public function create(){


        $this->pdf->SetLeftMargin(20);
        $this->pdf->SetTopMargin(20);

        $this->pdf->AddPage();
        $this->pdf->SetFont($this->fontFamily,'B', $this->fontSize);

        $this->addLogo();
        $this->addJobNumber();

        $this->addCustomerInfo();

        $this->addProductsInfo();

        // after all this you need to call either display() or exportFile() manually
    }

    // shows pdf in browser
    public function display(){
        return $this->pdf->Output();
    }

    // saves file locally
    public function export(){
        return $this->pdf->Output('S');
    }

    private function addLogo(){

        $imageName = dirname(__FILE__) . '/Variant_Malibu.png';
        $originalWidth = 474;
        $originalHeight = 189;

        $scale = 0.12;

        $width = $originalWidth * $scale;
        $height = $originalHeight * $scale;

        $x = $this->pdf->getLeftMargin();
        $y = 10;

        // Logo
        $this->pdf->Image($imageName, $x, $y, $width, $height);
//        $this->pdf->Cell($this->pdf->offsetWidth(), $height); // occupy space on right

        $this->pdf->Ln($height + 5);
    }

    private function addJobNumber(){


        $text = 'JOB - '.$this->orderNumber;

        $this->pdf->setFontSize(16);

        $this->pdf->Cell(0, 0, $text);
        $this->pdf->Ln(10);

        $this->pdf->setFontSize($this->fontSize);
    }

    private function addCustomerInfo(){

        $this->pdf->SetFontSize(9);

        if($this->orderDate !== null) $date = $this->orderDate;
        else $date = $this->getCurrentDate();

        $text4 = "Order Number: ".$this->orderNumber;
        $text5 = "Order Date: ".$date;
        $text6 = "Shipping Method: ".$this->shippingMethod;


        $data = [];
        // draws lines first in column
        $data[] = [$this->customerName, $text4];
        $data[] = [$this->address1, $text5];
        $data[] = [$this->address2, $text6];

        if(strlen($this->address3) > 0){
            $data[] = [$this->address3, ""];
        }

        $this->pdf->addressAndShippingTable($data, false);
        $this->pdf->Ln(10);

    }

    private function getCurrentDate(){
           return date("F j, Y"); // MonthName Day, YEAR
    }

    private function addProductsInfo(){

        $columnWidth = [
            $this->pdf->offsetWidth()* 0.7,
            $this->pdf->offsetWidth() * 0.3,
        ];

        $this->addProductsHeader($columnWidth);

        foreach($this->list as $product){
            if($this->checkForPageBreak()){
                $this->pdf->AddPage();
//                $this->addProductsHeader($columnWidth);
            }
            $this->addProduct(new Product($product), $columnWidth);

        }
    }

    private function addProductsHeader($columnWidth){

        $header = ['Product','Quantity'];

        $this->pdf->setFontSize(10);
        $this->pdf->ProductTableHeader($header, $columnWidth);

    }


    private function addProduct($product, $columnWidth)
    {

        $quantity = $product->getQuantity();

        // Color and font restoration
        $this->pdf->SetFillColor(255, 255, 255);
        $this->pdf->SetTextColor(0);
        $this->pdf->SetFont('');
        $this->pdf->SetLineWidth(.4);
        $this->pdf->SetDrawColor(200, 200, 200); // table color

        $imageHeight = 60; // mm (NOT PIXELS)
        $imageWidth =  $imageHeight * $product->getImageRatio();
        $imageX = $this->pdf->getLeftMargin();
        $imageY = $this->pdf->getY() + 2;
        $this->pdf->Image($product->getImage(), $imageX, $imageY, $imageWidth, $imageHeight);

        $this->pdf->setFontSize(10);
        $this->pdf->Cell($columnWidth[0] + $this->pdf->getStringWidth($quantity)*2, 7, $quantity, 0, 0, 'R');

        $this->pdf->Ln($imageHeight + 5);

        $this->pdf->setFontSize(10);

        $colorName = trim($product->getColorName());

        if(strlen($colorName) > 0){
            $colorText = "Color: ".$colorName;
            $this->pdf->Cell($columnWidth[0],2, $colorText,'0',0);
            $this->pdf->Ln(4);
        }

        $this->pdf->setFontSize(8);
        $textHeight = 3.5;
        $multiText = "";

        if($product->isSweater()) {
            $multiText .= "Size: " . $product->getSize() . " \r\n";
            $textHeight += 0.5;
        }

        $multiText .= "Printing Type: ".$product->getPrinting(). " \r\n";
        $multiText .= "Product Colors: ". $product->getColorCodesInline() . "\r\n";

        if($product->hasRibColor()){
            $multiText .= "Rib Color: ".$product->getRibColor(). " \r\n";
            $textHeight += 0.33;
        }
        if($product->hasCuffColor()){
            $multiText .= "Cuff Color: ".$product->getCuffColor(). " \r\n";
            $textHeight += 0.33;
        }
        if($product->hasCollarColor()){
            $multiText .= "Collar Color: ".$product->getCollarColor(). " \r\n";
            $textHeight += 0.33;
        }

        $this->pdf->MultiCell($columnWidth[0],$textHeight, $multiText);
        $this->pdf->Ln();

        $this->pdf->setFontSize(10);
        $colorText = "Letterman";
        $this->pdf->Cell($columnWidth[0],2, $colorText,'0',0);
        $this->pdf->Ln(4);

        $this->pdf->setFontSize(8);
        $multiText = "";
        if($product->hasSku()) $multiText .= "SKU: ".$product->getSku()." \r\n";
        $multiText .= "Weight: ".$product->getWeight(). " \r\n";
        $this->pdf->MultiCell($columnWidth[0],4, $multiText);
        $this->pdf->Ln();


//

        // Closing line
        $this->pdf->Cell(array_sum($columnWidth), 0 ,'','T');

        $this->pdf->Ln(3);

    }

    function checkForPageBreak(){

        $pageHeight = $this->pdf->GetPageHeight();
        $currentY = $this->pdf->getY();
        $boxSize = 100;

        if($currentY + $boxSize > $pageHeight){
            return true;
        }
        return false;

    }
}

