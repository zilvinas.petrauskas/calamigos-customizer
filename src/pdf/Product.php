<?php

require_once  'constants.php';

class Product{

    private $image = "dummy.png";
    private $imageRatio = 1;

    private $quantity = 1;
    private $color = "White Cotton";
    private $size = "XS - 1";
    private $printingType = "Knit to shape"; // default
    private $colorCodes = [];
    private $sku = "";
    private $type = SWEATER_TYPE;

    private $ribColor = "";
    private $cuffColor = "";
    private $collarColor = "";
    private $customText = [
        [
            "text" => "Some text",
            "color" => "Marina Blue",
            "colorCode" => "000000",
        ]
    ]; // this should be array for multiple texts (TODO - texts have their own color, need that too)


    public function __construct($productParams=[]){
        if(is_array($productParams)){
            foreach($productParams as $k=>$v)
                if( isset($this->{$k}) ) $this->{$k} = $v;
        }
    }

    public function isSweater(){ return $this->type === SWEATER_TYPE; }
    public function isBlanket(){ return $this->type === BLANKET_TYPE;}
    public function hasImage(){ return strlen($this->image) > 0; }
    public function getImage(){ return $this->image; }
    public function getImageRatio(){ return $this->imageRatio; }
    public function getColorName(){ return $this->color; }
    public function getQuantity(){ return $this->quantity; }
    public function getSize(){ return $this->size; }
    public function getPrinting(){ return $this->printingType; }
    public function getColorCodes(){ return $this->colorCodes; }
    public function getSku(){ return $this->sku; }
    public function hasSku(){ return strlen($this->sku) > 0; }

    public function hasRibColor(){ return $this->ribColor !== ""; }
    public function hasCuffColor(){ return $this->cuffColor !== ""; }
    public function hasCollarColor(){ return $this->collarColor !== ""; }
    public function getRibColor(){ return $this->ribColor; }
    public function getCuffColor(){ return $this->cuffColor; }
    public function getCollarColor(){ return $this->collarColor; }
    public function hasCustomText(){ return strlen($this->customText) > 0; }
    public function getCustomText(){ return $this->customText; }
    public function getSweaterWeight(){ return SWEATER_WEIGHT; }
    public function getBlanketWeight(){ return BLANKET_WEIGHT; }
    public function getWeight(){
        if($this->isSweater()){
            return SWEATER_WEIGHT;
        }else{
            return BLANKET_WEIGHT;
        }
    }
    public function getColorCodesInline(){
        $array = [];
        foreach($this->colorCodes as $k=>$v){
            if(is_int($k)){
                $array[] = "Color".$k." - ".$v;
            }else{
                $array[] = $k." - ".$v;
            }

        }
        return implode(", ", $array).";";
    }
}