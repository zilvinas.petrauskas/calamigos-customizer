<?php

require_once 'PackingSlip.php';

$productsList = [];

$product1 = [
//    'image' => 'dummy.png',
//      'imageRatio' => 1, // width to height ratio to display correct image
//    'quantity' => 1,
//    'color' => 'White Cotton', // text
//    'colorCodes' => ['Color1' => '#123123', 'Color2' => 'e8e8e8', 'Color3' => 'b5b5b5'], // array of all color codes used
//    'size' => '',
//    'printingType' => '',
//    'sku' => '',
//    'type' => SWEATER_TYPE, // SWEATER_TYPE or BLANKET_TYP
    'ribColor' => '#000000',
    'cuffColor' => '#000000',
    'collarColor' => '#000000',
];

$productsList[] = $product1; // add product to list


$packingSlip = new PackingSlip($productsList); // IMPORTANT - pass productsList to packing slip class

//$packingSlip->customerName = 'Firstname Lastname';
//$packingSlip->orderNumber = 123;
//$packingSlip->address1 = 123;
//$packingSlip->address2 = 123;
//$packingSlip->shippingMethod = 'Free Shipping';
//$packingSlip->orderDate = 'May 10, 2019'; // if not provided, code will generate today's date
$packingSlip->create();
$packingSlip->display();