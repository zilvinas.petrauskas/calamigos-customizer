<?php

require_once 'fpdf.php';

class PDF extends  FPDF{

    function Header(){
        // Logo
//        $this->Image('logo.png',10,6,30);
//        // Arial bold 15
//        $this->SetFont('Arial','B',15);
//        // Move to the right
//        $this->Cell(80);
//        // Title
//        $this->Cell(30,10,'Title',1,0,'C');
//        // Line break
//        $this->Ln(20);
    }

    function Footer(){
        // Position at 1.5 cm from bottom
//        $this->SetY(-15);
//        // Arial italic 8
//        $this->SetFont('Arial','I',8);
//        // Page number
//        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    public function offsetWidth(){
        return $this->getPageWidth() - $this->lMargin * 2;
    }

    function ProductTableHeader($header, $columnWidth){

        // Colors, line width and bold font
        $this->SetFillColor(0,0,0);
        $this->SetTextColor(255);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(.3);
        $this->SetFont('','B');

        // Header
//        $w = array(40, 35, 40, 45);
        if($header !== null) {
            for ($i = 0; $i < count($header); $i++)
                $this->Cell($columnWidth[$i], 7, $header[$i], 0, 0, 'L', true);

            $this->Ln();
        }

    }
    // Colored table
    function ProductTable($data, $columnWidth){


        // Color and font restoration
        $this->SetFillColor(255,255,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        $this->SetLineWidth(.4);
        $this->SetDrawColor(200,200,200); // table color

        // Data
        $fill = false;

        foreach($data as $row){

            $this->Cell($columnWidth[0],6,$row[0],'0',0);
            $this->Cell($columnWidth[1],6,$row[1],'0',0);
            $this->Ln();
//            $fill = !$fill;
        }
        // Closing line
        $this->Cell(array_sum($columnWidth), 0 ,'','B');
    }

    function addressAndShippingTable($data, $drawLines=true){

        $pageWidth = $this->offsetWidth();

        $columnWidth = [
            $pageWidth* 0.6,
            $pageWidth * 0.4,
        ];

        foreach($data as $r=>$row){
            foreach($row as $c=>$col)
                $this->Cell($columnWidth[$c], 5,$col, $drawLines === true ? 1 : 0);
            $this->Ln();
        }

    }

    function getLeftMargin(){
        return $this->lMargin;
    }
    function getTopMargin(){
        return $this->tMargin;
    }

}
