<?php

require_once 'PackingSlip.php';
require_once 'constants.php';

$productsList = [];

$product1 = [
    'image' => 'dummy-blanket.png',
      'imageRatio' => 4/3, // height to width ratio, meaning height/width and pass that scale number
//    'quantity' => 1,
    'color' => 'Fresh Orange', // text
    'colorCodes' => ['Top'=>'#gray', 'Middle' => '#e8e8e8', 'Text' => '#b5b5b5'], // array of all color codes used
    'printingType' => 'Just some test values',
    'sku' => '00011111',
    'type' => BLANKET_TYPE, // SWEATER_TYPE ('sweater') or BLANKET_TYPE ('blanket')
];

$productsList[] = $product1; // add product to list


$packingSlip = new PackingSlip($productsList); // IMPORTANT - pass productsList to packing slip class

$packingSlip->customerName = 'Firstname Lastname';
$packingSlip->orderNumber = 123456;
$packingSlip->address1 = "Address Line 1";
$packingSlip->address2 = "Address Lien 2";
$packingSlip->shippingMethod = 'FedEx Shipping';
//$packingSlip->orderDate = 'May 10, 2019'; // if not provided, code will generate today's date
$packingSlip->create();
$packingSlip->display();