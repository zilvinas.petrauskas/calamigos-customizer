<?php

class ParseCheckoutInfo{

    private $data;

    private $customerName = "";
    private $orderNumber = "";
    private $address1 = "";
    private $address2 = "";
    private $address3 = "";
    private $orderDate = null;
    private $shippingMethod = "";

    private $products = [];

    public function __construct($data){
        $this->data = $data;
    }

    /*
     * what we need for PDF?
     *
     * packing slip info
     * customerName
     * orderNumber
     * address1
     * address2
     * shoppingMethod
     * orderDate
     *
     * products info
     * image (screenshot)
     * imageRatio
     * quantity
     * color (name)
     * size
     * printingType
     * colorCodes[]
     * sku
     * type ('sweater' or 'blanket')
     * ribColor
     * cuffColor
     * collarColor
     * customText[]
     */

    public function parse(){

        // name
        $this->customerName = $this->getCustomerName($this->data['shipping_address']);

        // address
        $this->address1 = $this->getAddressLine1($this->data['shipping_address']);
        $this->address2 = $this->getAddressLine2($this->data['shipping_address']);
        $this->address3 = $this->getAddressLine3($this->data['shipping_address']);

        // shipping
        $this->shippingMethod = $this->getShippingMethod($this->data['shipping_lines']);

        // order number
        $this->orderNumber = $this->getOrderNumber($this->data);

        // order date
        $this->orderDate = $this->getOrderDate($this->data); // why? because webhook may come from yesterday, for example it was ordered at 11:59:59 pm, but webhook was sent on 12:00:01, jsut to prevenet confusion

        // parse products
        $this->products = $this->parseProducts($this->data['line_items']);
    }

    private function getOrderNumber($data){
        return str_replace("#","",$data['name']); // #1018
    }

    private function getOrderDate($data){
        $date_iso8601 = $data['processed_at']; //'2014-03-13T09:05:50.240Z';
        return date('Y-m-d', strtotime($date_iso8601));
    }

    private function getCustomerName($customer){

        return trim($customer['first_name']." ".$customer['last_name']);

    }

    private function getAddressLine1($customer){
        return implode(" ", [
            trim($customer['address1']." ".$customer['address2']).",",
            $customer['city']
        ]);
    }

    private function getAddressLine2($customer){
        return implode(" ", [
            $customer['province'].",",
            $customer['province_code'],
            $customer['zip']
        ]);
    }

    private function getAddressLine3($customer){
        return implode(" ",[
            $customer['country']
        ]);
    }

    private function getShippingMethod($array){

        $method = "";

        foreach($array as $k=>$v){
            if(isset($v) && is_array($v)){
                return $this->getShippingMethod($v);
            }else if($k == 'title'){
                $method = $v;
                break;
            }
        }

        return $method;

    }

    private function parseProducts($products){

        $array = [];

        foreach($products as $product){
            $readyData = [];
            $readyData['quantity'] = $product['quantity'];
            $readyData['size'] = $product['variant_title'];

            foreach($product['properties'] as $prop){
                switch($prop['name']){
                    case "Color": $readyData['color'] = $prop['value']; break;
                    // TODO add custom properties to shopify
                }
            }
        }

        return $array;
    }
}
