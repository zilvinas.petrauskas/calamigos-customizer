<?php

define('SHOPIFY_APP_SECRET', '76b965d2d074a70ca9833086942d033de5375428326f2b5885cc5d9c982877b6');

function verify_webhook($data, $hmac_header){
  $calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
  return hash_equals($hmac_header, $calculated_hmac);
}


?>