<?php

require_once 'webhooksConfig.php';

$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$data = file_get_contents('php://input');
$verified = verify_webhook($data, $hmac_header);
//error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result

$content = 'Webhook verified: '.var_export($verified, true) . PHP_EOL;
$content .= 'Cart data: '. PHP_EOL;
$content .= print_r($data, true) . PHP_EOL;
$this_directory = dirname(__FILE__);
$fp = fopen($this_directory . "/logs/_".time()."_cart.txt", "w");
fwrite($fp, $content); 
fclose($fp);