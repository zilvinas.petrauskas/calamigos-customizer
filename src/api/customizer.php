<?php
	
$allowedOrigin = "https://calamigosdrygoods.com"; // https://calamigosdrygoods.com

require_once '../pdf/PackingSlip.php';

header("Access-Control-Allow-Origin: ".$allowedOrigin);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header('Content-Type: application/json');
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])){
	header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	/* The 2 lines below are a workaround based on this SO answer: https://stackoverflow.com/questions/1282909/php-post-array-empty-upon-form-submission*/
	$rest_json = file_get_contents("php://input");
	$_POST = json_decode($rest_json, true);

	if(isset($_POST['product'])){
		handleCustomProduct($_POST);
	}else{
		var_dump($_POST);
		die('Permission Denied for POST');
	}

}elseif($_SERVER['REQUEST_METHOD'] == 'GET'){

	// things in GET mostly for testing stuff
	header('Content-Type: text/html; charset=utf-8');

	if( isset($_GET['phpversion']) ){

		die('PHP version: ' . phpversion());

	}else if( isset($_GET['url']) ){

		$currURL = getCurrentUrl();
		die($currURL);

	}else if( isset($_GET['origin']) ){

		die('Origin: ' . $_SERVER['HTTP_ORIGIN']);

	}else if( isset($_GET['testwrite'])){
		
		// test server permission if it allows to write files
		$testDir = 'test';
		$testFileName = 'test_file_write.txt';

		$testDirs = [
			dirname(__DIR__ .'/'. $testDir.'/'),
		];

		$errors = 0;

		foreach($testDirs as $k=>$v){
			$index = $k + 1;
			if ( ! is_writable($v)) {
				echo '#'.$index.' NOT WRITABLE: '.$v.'<br>';
				$errors++;
			}else{
				echo '#'.$index.' Writable: '.$v.'<br>';
			}
		}
		
		if($errors > 0){
			echo '#'.($index+1).' Test file not written. Fix permissions on server before continueing';
			die('');
		}

		if (!is_dir($testDir)) mkdir($testDir, 0777, true);

		$testText = "just testing if I can write a file into server folder";
		$testFilePath = $testDir . "/" . $testFileName;
		$testFile = fopen($testFilePath, "w");
		fwrite($testFile, $testText);
		fclose($testFile);
		echo '#'.($index+1).' Test file written, check server if folder <'.$testDir.'> exists and it has a file <'.$testFileName.'> inside';
		die('');


	}else{
		die('Permission Denied for GET');
	}

}else{
	die('Unknown request method: ' . $_SERVER['REQUEST_METHOD']);
}

// return a safe to write string
function makeSafe($string){
	return filter_var( trim($string), FILTER_SANITIZE_STRING);
}

// always pass $data along 
// returns $data
function parseProductData(&$data, $input, $name=""){
	foreach($input as $key=>$value){
		if(!empty($value) && is_array($value)){
			parseProductData($data, $value, $key);
		}else{
			if($name == "colors"){
				$data[] = makeSafe($key)." color: ".makeSafe($value);
			}else{
				$data[] = makeSafe($key).": ".makeSafe($value);
			}
			
		}
	}
}

function handleCustomProduct($input){

	
	$orderNumber = time() . rand(10000, 99999);

	$path = 'orders/' . $orderNumber;

	$currentServer = getCurrentUrl();

	$privatePath = __DIR__ . '/'. $path; // use this dir to store files locally on server
	$publicPath = $currentServer . $path; // this is a public location of file 
	
	
	if (!is_dir($privatePath)) mkdir($privatePath, 0777, true);

	$return = [
		'orderNumber' => $orderNumber, /* NEVER REMOVE THIS ONE, when it sends it response this order number gets added to hidden input for later use */ 
	];
	

	if(isset($input['product']) && is_array($input['product'])){

		$data = [];
		$orderText = "";

		// parse product data into array
		parseProductData($data, $input['product']);

		// convert array into text lines
		$orderText = implode($data, "\r\n");

		$orderFilePath = $path . '/' . $orderNumber . "_1.txt";
		$orderFile = fopen($orderFilePath, "w");
		fwrite($orderFile, $orderText);
		fclose($orderFile);

		$return['file'] = $orderFilePath;
		$return['safeData'] = $data;
		

	}
	
	
	if (isset($input['masks'])){

		$masks = $input['masks'];
		$return['masksCreated'] = [];
		$return['imagesOnline'] = [];

		foreach ($masks as $name => $value){

			$imageData = null;

			if (!is_null($masks[$name]['PNGFile'])){

				$imageData = base64_decode($masks[$name]['PNGFile']);
				$extension = "png";

				$filePath = $orderNumber . '_' . $name . '.' . $extension;

				file_put_contents($path . '/' . $filePath, $imageData);
				$return['masksCreated'][] = $filePath;

			}

//			if (!is_null($masks[$name]['BMPFile'])){
//
//				$imageData = base64_decode($masks[$name]['BMPFile']);
//				$extension = "bmp";
//
//				$filePath = $orderNumber . '_' .$name . '.' . $extension;
//
//				file_put_contents($path . '/' . $filePath, $imageData);
//				$return['masksCreated'][] = $filePath;
//			}


		}

		
	}



    // screenshot - image, width. height
    $imageData = $input['screenshot']['image'];
    $imageData = str_replace('data:image/png;base64,', '', $imageData);
    $imageData = str_replace(' ', '+', $imageData);
    $imageData = base64_decode($imageData);

    $extension = "png";

    $filePath = $orderNumber . '_screenshot' . '.' . $extension;
    $screenshotPath = $path . '/' . $filePath;
    file_put_contents($screenshotPath, $imageData);
    $return['screenshot'] = $publicPath .'/'. $filePath;


	if(isset($input['product']) && isset($input['product']['colors'])){

		$inlineColors = [];
		$productColors = $input['product']['colors'];
		foreach($productColors as $colorName=>$colorCode){
			if(!empty(trim($colorCode))){
				$inlineColors[] = $colorCode;
			}
		}

		$return['colors'] = $productColors;
		$return['colorsInline'] = implode('|',$inlineColors);
	}


    $return['PDF'] = makePDF($input, $screenshotPath, $path);
	


	echo json_encode($return);

}

function isLocalhost(){
	$whitelist = array(
		'127.0.0.1',
		'::1'
	);
	
	return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
}

function getCurrentUrl(){
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; // $_SERVER[REQUEST_URI]
    $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';

    return $actual_link . $basepath; // . basename(__FILE__);
}

function makePDF($input, $image, $destinationFolder){

    $filename = 'packingslip.pdf'; // name of pdf file with extension
    $product = $input['product'];

	$productList = [];
	$productList[] = [
            'image' => $image,
            'imageRatio' => $input['screenshot']['width'] / $input['screenshot']['height'],
            'quantity' => $product['quantity'],
            'color' => '', // we dont have this
            'colorCodes' => $product['colors'],
            'size' => isset($product['size']) ? $product['size'] : '',
//            'printingType' => '', // you may want to add this yourself
//            'sku' => '',
            'type' => $product['type'], // SWEATER_TYPE or BLANKET_TYP
    ];

	// need to pass information to existing class
	$packingSlip = new PackingSlip($productList);
	//$packingSlip->customerName = 'Firstname Lastname';
	//$packingSlip->orderNumber = 123;
	//$packingSlip->address1 = 123;
	//$packingSlip->address2 = 123;
	//$packingSlip->shippingMethod = 'Free Shipping';
	//$packingSlip->orderDate = 'May 10, 2019'; // if not provided, code will generate today's date
	$packingSlip->create();
    $pdfData = $packingSlip->export();

    $pdfFilePath = $destinationFolder.'/'.$filename;
	file_put_contents($pdfFilePath, $pdfData);


	return $pdfFilePath;

}